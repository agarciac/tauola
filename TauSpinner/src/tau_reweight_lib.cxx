#include "TauSpinner/tau_reweight_lib.h"
#include "TauSpinner/nonSM.h"
#include "TauSpinner/vbfdistr.h"
#include <cstring>
using std::memcpy;

#include "Tauola/TauolaParticlePair.h"
using namespace Tauolapp;

namespace TauSpinner {

// ===================================================================
// ATLAS: Start of new class based implementation: TauSpinnerWeighter
// ===================================================================
TauSpinnerWeighter::TauSpinnerWeighter()
    : CMSENE(7000.0)
    , Ipp(true)
    , Ipol(1)
    , nonSM2(0)
    , nonSMN(0)
    , relWTnonSM(1)
    , WTnonSM(1.0)
    , Polari(0.0)
    , IfHiggs(false)
    , WTamplit(1)
    , WTamplitP(1)
    , WTamplitM(1)
    , IfHsimple(0)
    , XMH(125.0)
    , XGH(1.0)
    , Xnorm(0.15)
    , RXX(0.0)
    , RYY(0.0)
    , RXY(0.0)
    , RYX(0.0)
    , RzXX(0.0)
    , RzYY(0.0)
    , RzXY(0.0)
    , RzYX(0.0)
    , R11(0.0)
    , R22(0.0)
    , R12(0.0)
    , R21(0.0)
    , DEBUGVBF(false)
    , _QCDdefault(1)
    , _QCDvariant(1) 
    , alphasModif(NULL)
    , vbfdistrModif(NULL)
{
    nonSM_bornZ  = std::bind(&TauSpinnerWeighter::default_nonSM_born,
                             this,
                             std::placeholders::_1,
                             std::placeholders::_2,
                             std::placeholders::_3,
                             std::placeholders::_4,
                             std::placeholders::_5,
                             std::placeholders::_6);
    nonSM_bornH = std::bind(&TauSpinnerWeighter::default_nonSM_bornH,
                            this,
                            std::placeholders::_1,
                            std::placeholders::_2,
                            std::placeholders::_3,
                            std::placeholders::_4,
                            std::placeholders::_5,
                            std::placeholders::_6);

}

// ATLAS: undeclared functions from tau_reweight_lib.cxx

double TauSpinnerWeighter::f(double x, int ID, double SS, double cmsene)
// PDF's parton density function divided by x;
// x - fraction of parton momenta
// ID flavour of incoming quark
// SS scale of hard process
// cmsene center of mass for pp collision.
{
  // LHAPDF manual: http://projects.hepforge.org/lhapdf/manual
  //  double xfx(const double &x;, const double &Q;, int fl);
  // returns xf(x, Q) for flavour fl - this time the flavour encoding
  // is as in the LHAPDF manual...
  // -6..-1 = tbar,...,ubar, dbar
  // 1..6 = duscbt
  // 0 = g
  // xfx is the C++ wrapper for fortran evolvePDF(x,Q,f)

  // note that SS=Q^2, make the proper choice of PDFs arguments.
  return LHAPDF::xfx(x, sqrt(SS), ID)/x;

  //return x*(1-x);

}

  // Calculates Born cross-section summed over final taus spins.
  // Input parameters: 
  // incoming flavour                    ID  
  // invariant mass^2                    SS  
  // scattering angle                    costhe
  // Hidden input (type of the process): IfHiggs, IfHsimple
double TauSpinnerWeighter::sigborn(int ID, double SS, double costhe)
{
  //  cout << "ID : " << ID << " HgsWTnonSM = " << HgsWTnonSM << " IfHsimple = " << IfHsimple << endl;
  // BORN x-section.
  // WARNING: overall sign of costheta must be fixed
  int tauID = 15;

  // case of the Higgs boson
    if (IfHiggs) {
      double SIGggHiggs=0.;
      // for the time being only for  gluon it is non-zero. 
      if(ID==0){        
        int IPOne =  1;
        int IMOne = -1;
        SIGggHiggs=disth_(&SS, &costhe, &IPOne, &IPOne)+disth_(&SS, &costhe, &IPOne, &IMOne)+
  	           disth_(&SS, &costhe, &IMOne, &IPOne)+disth_(&SS, &costhe, &IMOne, &IMOne);


        double PI=3.14159265358979324;
	SIGggHiggs *=  XMH * XMH * XMH *XGH /PI/ ((SS-XMH*XMH)*(SS-XMH*XMH) + XGH*XGH*XMH*XMH);   
	//	cout << "JK: SIGggHiggs = " << SS << " " << XMH << " " << XGH << " " <<  XMH * XMH * XMH * XGH /PI/ ((SS-XMH*XMH)*(SS-XMH*XMH) + XGH*XGH*XMH*XMH) << " " << SIGggHiggs << endl;

        if(IfHsimple==1) SIGggHiggs =  Xnorm * XMH * XGH /PI/ ((SS-XMH*XMH)*(SS-XMH*XMH) +XGH*XGH*XMH*XMH);
	//	cout << "ZW: SIGggHiggs = " << SS << " " << costhe << " " << SIGggHiggs << endl;
      }
    return SIGggHiggs;
    }

  // case of Drell-Yan

  if (ID==0) return 0.0 ;   // for the time being for gluon it is zero.
  if (ID>0) initwk_( &ID, &tauID, &SS);
  else
  {
    ID = -ID;
    initwk_( &ID, &tauID, &SS);
  }

  int    iZero = 0;
  double dOne  =  1.0;
  double dMOne = -1.0;
  // sum DY Born over all tau helicity configurations:
  return ( t_born_(&iZero, &SS, &costhe, &dOne , &dOne) + t_born_(&iZero, &SS, &costhe, &dOne , &dMOne)
	   + t_born_(&iZero, &SS, &costhe, &dMOne, &dOne) + t_born_(&iZero, &SS, &costhe, &dMOne, &dMOne))/SS/123231.; 
// overall norm. factor .../SS/123231  most probably it is alpha_QED**2/pi/2/SS is from comparison between Born we use and Born used in Warsaw group. 
}


// ATLAS: functions from tau_reweight_lib.cxx

/*******************************************************************************
  Initialize TauSpinner

  Print info and set global variables
*******************************************************************************/
void TauSpinnerWeighter::initialize_spinner(bool _Ipp, int _Ipol, int _nonSM2, int _nonSMN, double _CMSENE)
{
  Ipp    = _Ipp;
  Ipol   = _Ipol;
  nonSM2 = _nonSM2;
  nonSMN = _nonSMN;

  CMSENE = _CMSENE;

  cout<<" ------------------------------------------------------"<<endl;
  cout<<" TauSpinner v2.0.1"<<endl;
  cout<<" -----------------"<<endl;
  cout<<"   19.May.2016    "<<endl;
  cout<<" by Z. Czyczula (until 2015), T. Przedzinski, E. Richter-Was, Z. Was,"<<endl;
  cout<<"  matrix elements implementations "<<endl;
  cout<<"  also J. Kalinowski and W. Kotlarski"<<endl;
  cout<<" ------------------------------------------------------"<<endl;
  cout<<" Ipp - true for pp collision; otherwise polarization"<<endl;
  cout<<"       of individual taus from Z/gamma* is set to 0.0"<<endl;
  cout<<" Ipp    = "<<Ipp<<endl;
  cout<<" CMSENE - used in PDF calculations; only if Ipp = true"<<endl;
  cout<<"          and only for Z/gamma*"<<endl;
  cout<<" CMSENE = "<<CMSENE<<endl;
  cout<<" Ipol - relevant for Z/gamma* decays "<<endl;
  cout<<" 0 - events generated without spin effects                "<<endl;
  cout<<" 1 - events generated with all spin effects               "<<endl;
  cout<<" 2 - events generated with spin correlations and <pol>=0  "<<endl;
  cout<<" 3 - events generated with spin correlations and"<<endl;
  cout<<"     polarization but missing angular dependence of <pol>"<<endl;
  cout<<" Ipol    = "<<Ipol<<endl;
  cout<<" Ipol - relevant for Z/gamma* decays "<<endl;
  cout<<" NOTE: For Ipol=0,1 algorithm is identical.               "<<endl;
  cout<<"       However in user program role of wt need change.    "<<endl;
  cout<<" nonSM2  = "<<nonSM2<<endl;
  cout<<" 1/0 extra term in cross section, density matrix on/off   "<<endl;
  cout<<" nonSMN  = "<<nonSMN<<endl;
  cout<<" 1/0 extra term in cross section, for shapes only? on/off "<<endl;
  cout<<" note KEY - for options of matrix elements calculations   "<<endl;
  cout<<"            in cases of final states  with two jets       "<<endl;
  cout<<" ------------------------------------------------------   "<<endl;
}

/*******************************************************************************
  Set flag for calculating relative(NONSM-SM)/absolute weight for X-section
  calculated as by product in longitudinal polarization method.
  1: relWTnonSM is relative to SM (default)
  0: absolute
*******************************************************************************/
void TauSpinnerWeighter::setRelWTnonSM(int _relWTnonSM)
{
  relWTnonSM = _relWTnonSM;
}

/*******************************************************************************
  Set Higgs mass, width and normalization of Higgs born function
  Default is mass = 125, width = 1.0, normalization = 0.15
*******************************************************************************/
  void TauSpinnerWeighter::setHiggsParameters(int jak, double mass, double width, double normalization)
{
  IfHsimple=jak;
  XMH   = mass;
  XGH   = width;
  Xnorm = normalization;
}

/*******************************************************************************
  Set transverse components of Higgs spin density matrix
*******************************************************************************/
void TauSpinnerWeighter::setHiggsParametersTR(double Rxx, double Ryy, double Rxy, double Ryx)
{
  
  RXX = Rxx;
  RYY = Ryy;
  RXY = Rxy;
  RYX = Ryx;
}


/*******************************************************************************
  Set coefficients for transverse components of Z/gamma spin density matrix
*******************************************************************************/
void TauSpinnerWeighter::setZgamMultipliersTR(double Rxx, double Ryy, double Rxy, double Ryx)
{
  RzXX = Rxx;
  RzYY = Ryy;
  RzXY = Rxy;
  RzYX = Ryx;
}

/*******************************************************************************
  Get  transverse components of Z/gamma spin density matrix
*******************************************************************************/
void TauSpinnerWeighter::getZgamParametersTR(double &Rxx, double &Ryy, double &Rxy, double &Ryz)
{
  Rxx = R11;
  Ryy = R22;
  Rxy = 0.0;
  Ryz = 0.0;
}

/*******************************************************************************
  Get Higgs mass, width and normalization of Higgs born function
*******************************************************************************/
void TauSpinnerWeighter::getHiggsParameters(double *mass, double *width, double *normalization)
{
  *mass          = XMH;
  *width         = XGH;
  *normalization = Xnorm;
}

/*******************************************************************************
  Set type of spin treatment used in the sample
  Ipol = 0   sample was not polarised
  Ipol = 1   sample was having complete longitudinal spin effects 
  Ipol = 2   sample was featuring longitudinal spin correlations only,
             but not dependence on polarisation due to couplings of the Z
  Ipol = 3   as in previous case, but only angular dependence of spin polarisation
             was missing in the sample
*******************************************************************************/
void TauSpinnerWeighter::setSpinOfSample(int _Ipol)
{
  Ipol = _Ipol;
}

/*******************************************************************************
  Turn nonSM calculation of Born cross-section on/off
*******************************************************************************/
void TauSpinnerWeighter::setNonSMkey(int _key)
{
  nonSM2 = _key;
}

/*******************************************************************************
  Get nonSM weight
*******************************************************************************/
double TauSpinnerWeighter::getWtNonSM()
{
  return WTnonSM;
}
/*******************************************************************************
  Get weights for tau+ tau- decay matrix elements
*******************************************************************************/
double TauSpinnerWeighter::getWtamplitP(){return WTamplitP;}
double TauSpinnerWeighter::getWtamplitM(){return WTamplitM;}

/*******************************************************************************
  Get tau spin (helicities of tau+ tau- are 100% correlated)
  Use after sample is reweighted to obtain information on attributed  tau 
  longitudinal spin projection.
*******************************************************************************/
double TauSpinnerWeighter::getTauSpin()
{
  return Polari;
}

/*******************************************************************************
  Calculate weights, case of event record vertex like W -> tau nu_tau decay.
  Function for W+/- and H+/-

  Determines decay channel, calculates all necessary components for 
  calculation of all weights, calculates weights.
  Input:        X four momentum may be larger than sum of tau nu_tau, missing component
                is assumed to be QED brem
  Hidden input: none
  Hidden output: WTamplitM or WTamplitP of tau decay (depending on tau charge)
                 NOTE: weight for sp_X production matrix elements is not calculated 
                 for decays of charged intermediate W+-/H+-! 
  Explicit output: WT spin correlation weight 
*******************************************************************************/
double TauSpinnerWeighter::calculateWeightFromParticlesWorHpn(SimpleParticle &sp_X, SimpleParticle &sp_tau, SimpleParticle &sp_nu_tau, vector<SimpleParticle> &sp_tau_daughters)
{
  // Create Particles from SimpleParticles

  Particle X     (     sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
  Particle tau   (   sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
  Particle nu_tau(sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

  vector<Particle> tau_daughters;

  // tau pdgid
  int tau_pdgid = sp_tau.pdgid();

  // Create vector of tau daughters
  for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
  {
    Particle pp(sp_tau_daughters[i].px(),
                sp_tau_daughters[i].py(),
                sp_tau_daughters[i].pz(),
                sp_tau_daughters[i].e(),
                sp_tau_daughters[i].pdgid() );

    tau_daughters.push_back(pp);
  }

  double phi2 = 0.0, theta2 = 0.0;

  //  To calcluate matrix elements TAUOLA need tau decay products in tau rest-frame: we first boost all products
  //  to tau rest frame (tau nu_tau of W decay along z-axis, intermediate step for boost is tau nu_tau (of W decay) rest-frame), 
  //  then rotate to have neutrino from tau decay along z axis; 
  //  calculated for that purpose angles phi2, theta2 are stored for rotation back of HH
  prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);


  //  Identify decay channel and then calculate polarimetric vector HH; calculates also WTamplit
  double *HH = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);
 
  double sign = 1.0;  // tau from W is 100 % polarized, also from charged Higgs (but with opposite sign)
  if     ( abs(sp_X.pdgid()) == 24 ) { sign= 1.0; } 
  else if( abs(sp_X.pdgid()) == 37 ) { sign=-1.0; } 
  else
  {
    cout<<"wrong sp_W/H.pdgid()="<<sp_X.pdgid()<<endl;
    exit(-1);
  }
  if     (sp_X.pdgid() > 0 )   
    {WTamplitM = WTamplit;}   // tau- decay matrix element^2, spin averaged.
  else
    {WTamplitP = WTamplit;}   // tau+ decay matrix element^2, spin averaged.

  // spin correlation weight. Tau production matrix element is represented by `sign'
  double WT   = 1.0+sign*HH[2];     // [2] means 'pz' component

  // Print out some info about the channel
  DEBUG
  (
    cout<<tau_pdgid<<" -> ";
    for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
    cout<<" (HH: "<<HH[0]<<" "<<HH[1]<<" "<<HH[2]<<" "<<HH[3]<<") WT: "<<WT<<endl;
  )

  // TP:31Nov2013 checks of possible problems: weight outside theoretically allowed range
  if (WT<0.0) {
    printf("TauSpinner::calculateWeightFromParticlesWorHpn WT is: %13.10f. Setting WT = 0.0\n",WT);
    WT = 0.0;
   }

  if (WT>2.0) {
    printf("Tauspinner::calculateWeightFromParticlesWorHpn WT is: %13.10f. Setting WT = 2.0\n",WT);
    WT = 2.0;
  }

  delete HH;
  
  return WT;
}

/*******************************************************************************
  Calculate weights, case of event record vertex like Z/gamma/H ... -> tau tau decay.

  Determine decay channel, calculates all necessary components for 
  calculation of all weights, calculates weights.

  Input:        X four momentum may be larger than sum of tau1 tau2, missing component
                is assumed to be QED brem

  Hidden input:  relWTnonS, nonSM2 (used only in  getLongitudinalPolarization(...) )
  Hidden output: WTamplitM or WTamplitP of tau1 tau2 decays
                 weight for sp_X production matrix element^2 is calculated inside 
                 plzap2
                 Polari - helicity attributed to taus, 100% correlations between tau+ and tau-
                 WTnonSM  
  Explicit output: WT spin correlation weight 
*******************************************************************************/
double TauSpinnerWeighter::calculateWeightFromParticlesH(SimpleParticle &sp_X, SimpleParticle &sp_tau1, SimpleParticle &sp_tau2, vector<SimpleParticle> &sp_tau1_daughters, vector<SimpleParticle> &sp_tau2_daughters)
{
  //cout << "sp_tau1_daughters = " << sp_tau1_daughters.size() << endl;
  //cout << "sp_tau2_daughters = " << sp_tau2_daughters.size() << endl;
  SimpleParticle         sp_tau;
  SimpleParticle         sp_nu_tau;
  vector<SimpleParticle> sp_tau_daughters;


  // First we calculate HH for tau+  
  // We enforce that sp_tau is tau+ so the 'nu_tau' is tau-
  if (sp_tau1.pdgid() == -15 )
  {
    sp_tau           = sp_tau1;
    sp_nu_tau        = sp_tau2;
    sp_tau_daughters = sp_tau1_daughters;
  }
  else
  {
    sp_tau           = sp_tau2;
    sp_nu_tau        = sp_tau1;
    sp_tau_daughters = sp_tau2_daughters;
  }

  double *HHp, *HHm;
  
  // We use artificial if(true){... } construction to separate namespace for tau+ and tau-
  if(true) 
  {
    // Create Particles from SimpleParticles
    Particle X     (      sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
    Particle tau   (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
    Particle nu_tau( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

    vector<Particle> tau_daughters;

    // tau pdgid
    int tau_pdgid = sp_tau.pdgid();

    // Create list of tau daughters
    for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
    {
      Particle pp(sp_tau_daughters[i].px(),
                  sp_tau_daughters[i].py(),
                  sp_tau_daughters[i].pz(),
                  sp_tau_daughters[i].e(),
                  sp_tau_daughters[i].pdgid() );

      tau_daughters.push_back(pp);
    }

    double phi2 = 0.0, theta2 = 0.0;

    //  To calculate matrix elements TAUOLA need tau decay products in tau rest-frame: we first boost all products
    //  to tau rest frame (tau other tau  of Z/H decay along z-axis, intermediate step for boost is tau-tau pair rest-frame), 
    //  then rotate to have neutrino from tau decay along z axis; 
    //  calculated for that purpose angles phi2, theta2 are stored for rotation back of HHp
    prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);
 



    //  Identify decay channel and then calculate polarimetric vector HH; calculates also WTamplit
    HHp = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);
    HHp_.setPx(HHp[0]);
    HHp_.setPy(HHp[1]);
    HHp_.setPz(HHp[2]);
    HHp_.setE(HHp[3]);

    DEBUG
    (
      cout<<tau_pdgid<<" -> ";
      for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
      cout<<" (HHp: "<<HHp[0]<<" "<<HHp[1]<<" "<<HHp[2]<<" "<<HHp[3]<<") ";
      cout<<endl;
    )

    WTamplitP = WTamplit;
  } // end of if(true);  for tau+



  // Second we calculate HH for tau-  
  // We enforce that sp_tau is tau- so the 'nu_tau' is tau+
  if(sp_tau1.pdgid() == 15 )
  {
    sp_tau           = sp_tau1;
    sp_nu_tau        = sp_tau2;
    sp_tau_daughters = sp_tau1_daughters;
  }
  else
  {
    sp_tau           = sp_tau2;
    sp_nu_tau        = sp_tau1;
    sp_tau_daughters = sp_tau2_daughters;
  }
  
  // We use artificial if(true){... } construction to separate namespace for tau+ and tau-
  if(true)
  {
    // Create Particles from SimpleParticles
    Particle X     (      sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
    Particle tau   (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
    Particle nu_tau( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

    vector<Particle> tau_daughters;

    // tau pdgid
    int tau_pdgid = sp_tau.pdgid();

    // Create list of tau daughters
    for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
    {
      Particle pp(sp_tau_daughters[i].px(),
                  sp_tau_daughters[i].py(),
                  sp_tau_daughters[i].pz(),
                  sp_tau_daughters[i].e(),
                  sp_tau_daughters[i].pdgid() );

      tau_daughters.push_back(pp);
    }

    double phi2 = 0.0, theta2 = 0.0;


    //  To calculate matrix elements TAUOLA need tau decay products in tau rest-frame: we first boost all products
    //  to tau rest frame (tau other tau  of Z/H decay along z-axis, intermediate step for boost is tau-tau pair rest-frame), 
    //  then rotate to have neutrino from tau decay along z axis; 
    //  calculated for that purpose angles phi2, theta2 are stored for rotation back of HHm
    prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);


    //  Identify decay channel and then calculate polarimetric vector HHm; calculates also WTamplit
    HHm = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);
    HHm_.setPx(HHm[0]);
    HHm_.setPy(HHm[1]);
    HHm_.setPz(HHm[2]);
    HHm_.setE(HHm[3]);

    DEBUG
    (
      cout<<tau_pdgid<<" -> ";
      for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
      cout<<" (HHm: "<<HHm[0]<<" "<<HHm[1]<<" "<<HHm[2]<<" "<<HHm[3]<<") ";
      cout<<endl;
    )

    WTamplitM = WTamplit; 
  } // end of if(true);  for tau-

  // CALCULATION OF PRODUCTION MATRIX ELEMENTS, THEN SPIN WEIGHTS AND FURTHER HIDDEN OUTPUTS

  // sign, in ultrarelativistic limit it is  component of spin density matrix: 
  // longitudinal spin correlation for intermediate vector state gamma*, 
  // for Z etc. sign= +1; for Higgs, other scalars, pseudoscalars sign= -1
  double sign = 1.0; 
  if(sp_X.pdgid() == 25) { sign=-1.0; } 
  if(sp_X.pdgid() == 36) { sign=-1.0; }
  if(sp_X.pdgid() ==553) { sign=-1.0; }  // upsilon(1s) can be treated as scalar

  double WT = 0.0;

  Polari = 0.0;  
  
  if(sign == -1.0) // Case of scalar
  {
    double S = sp_X.e()*sp_X.e() - sp_X.px()*sp_X.px() - sp_X.py()*sp_X.py() - sp_X.pz()*sp_X.pz();
    IfHiggs=true; //global variable
    double pol = getLongitudinalPolarization(S, sp_tau, sp_nu_tau);  // makes sense only for nonSM otherwise NaN

    if(nonSM2==1) // WARNING it is for spin=2 resonance!!
      {
     
      double corrX2;
      double polX2;

      // NOTE: in this case, sp_nu_tau is the 2nd tau
      //      nonSMHcorrPol(S, sp_tau, sp_nu_tau, &corrX2, &polX2); // for future use
      //                                                          WARNING: may be for polX2*HHm[2] we need to fix sign!
      polX2=pol;
      corrX2=-sign; // if X2 is of spin=2, spin correlation like for Z, we use RzXX,RzYY,RzXY,RzYX as transverse components of density matrix 
      
      WT = 1.0+corrX2*HHp[2]*HHm[2]+polX2*HHp[2]+polX2*HHm[2] + RzXX*HHp[0]*HHm[0] + RzYY*HHp[1]*HHm[1] + RzXY*HHp[0]*HHm[1] + RzYX*HHp[1]*HHm[0];

      // we separate cross section into helicity parts. From this, we attribute helicity states to taus: ++ or -- 
      double RRR = Tauola::randomDouble();  
      Polari=1.0;
      if (RRR<(1.0+polX2)*(1.0+corrX2*HHp[2]*HHm[2]+HHp[2]+HHm[2])/(2.0+2.0*corrX2*HHp[2]*HHm[2]+2.0*polX2*HHp[2]+2.0*polX2*HHm[2])) Polari=-1.0;    
      }
    else  // case of Higgs
      {
      WT = 1.0 + sign*HHp[2]*HHm[2] + RXX*HHp[0]*HHm[0] + RYY*HHp[1]*HHm[1] + RXY*HHp[0]*HHm[1] + RYX*HHp[1]*HHm[0];

      //  we separate cross section into helicity parts. From this, we attribute helicity states to taus: +- or -+ 
      double RRR = Tauola::randomDouble();  
      Polari=1.0;
      if (RRR<(1.0+sign*HHp[2]*HHm[2]+HHp[2]-HHm[2])/(2.0+2.0*sign*HHp[2]*HHm[2])) Polari=-1.0;
      }
  }
  else   // Case of Drell Yan
  { 

    double S = sp_X.e()*sp_X.e() - sp_X.px()*sp_X.px() - sp_X.py()*sp_X.py() - sp_X.pz()*sp_X.pz();

    // Get Z polarization
    // ( Variable names are misleading! sp_tau is tau+ and sp_nu_tau is tau- )

    IfHiggs=false;
    double pol = getLongitudinalPolarization(S, sp_tau, sp_nu_tau);


    WT = 1.0+sign*HHp[2]*HHm[2]+pol*HHp[2]+pol*HHm[2] + RzXX*R11*HHp[0]*HHm[0] - RzYY*R22*HHp[1]*HHm[1] + RzXY*R12*HHp[0]*HHm[1] + RzYX*R21*HHp[1]*HHm[0]; 
    // in future we may need extra factor for wt which is
    //     F=PLWEIGHT(IDE,IDF,SVAR,COSTHE,1)
    // but it is then non standard version of the code.

    // to correct when in the sample  only spin corr. are in, but no polarization
    if(Ipol==2) WT = WT/(1.0+sign*HHp[2]*HHm[2]); 

    // to correct sample when  corr. are in,  pol. is in, but angular
    // dependence of pol is missing.
    if(Ipol==3)
    {
      // valid for configurations close to Z peak, otherwise approximation is at use.
      double polp1 = plzap2(11,sp_tau.pdgid(),S,0.0);
      double pol1 =(2*(1-polp1)-1) ;
      WT = WT/(1.0+sign*HHp[2]*HHm[2]+pol1*HHp[2]+pol1*HHm[2]); 
    } 

    //  we separate cross section into helicity parts. From this, we attribute helicity states to taus: ++ or -- 
    double RRR = Tauola::randomDouble();  
    Polari=1.0;
    if (RRR<(1.0+pol)*(1.0+sign*HHp[2]*HHm[2]+HHp[2]+HHm[2])/(2.0+2.0*sign*HHp[2]*HHm[2]+2.0*pol*HHp[2]+2.0*pol*HHm[2])) Polari=-1.0; 
  }

  // Print out some info about the channel
  DEBUG( cout<<" WT: "<<WT<<endl; )

  if (WT<0.0) {
    printf("Tauspinner::calculateWeightFromParticlesH WT is: %13.10f. Setting WT = 0.0\n",WT);
    WT = 0.0; // SwB:23Feb2013
   }

  if (WT>4.0) {
    printf("Tauspinner::calculateWeightFromParticlesH WT is: %13.10f. Setting WT = 4.0\n",WT);
    WT = 4.0; // SwB:23Feb2013
  }

  if( WT>4.0 || WT<0.0)
  {
    cout<<"Tauspinner::calculateWeightFromParticlesH ERROR: Z/gamma* or H, and WT not in range [0,4]."<<endl;
    exit(-1);
  }

  if (sign==-1.0 && nonSM2!=1) {
    if (WT>2.0) {
      WT = 2.0; // SwB:26Feb2013
      cout << "Tauspinner::calculateWeightFromParticlesH Setting WT to be 2.0" << endl;
    }
  }

  if( sign==-1.0 && (WT>2.0 || WT<0.0) && nonSM2!=1)
  {
    cout<<"Tauspinner::calculateWeightFromParticlesH ERROR: H and WT not in range [0,2]."<<endl;
    exit(-1);
  }

  delete[] HHp;
  delete[] HHm;
  
  return WT;
}

/*******************************************************************************
  Prepare kinematics for HH calculation
  
  Boost particles to effective bozon rest frame, and rotate them so that tau is on Z axis.
  Then rotate again with theta2 phi2 so neutrino from tau decay is along Z.
*******************************************************************************/
void TauSpinnerWeighter::prepareKinematicForHH(Particle &tau, Particle &nu_tau, vector<Particle> &tau_daughters, double *phi2, double *theta2)
{
  Particle P_QQ( tau.px()+nu_tau.px(), tau.py()+nu_tau.py(), tau.pz()+nu_tau.pz(), tau.e()+nu_tau.e(), 0 );

  //cout<<endl<<"START: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);
  
  // 1) boost tau, nu_tau and tau daughters to rest frame of P_QQ

  tau.boostToRestFrame(P_QQ);
  nu_tau.boostToRestFrame(P_QQ);

  for(unsigned int i=0; i<tau_daughters.size();i++)
    tau_daughters[i].boostToRestFrame(P_QQ);

  //cout<<endl<<"AFTER 1: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);

  // 2) Rotate tau, nu_tau~, tau daughters to frame where tau is along Z
  //    We set accompanying neutino in direction of Z+

  double phi = tau.getAnglePhi();

  tau.rotateXY(-phi);

  double theta   = tau.getAngleTheta();

  tau.rotateXZ(M_PI-theta);

  nu_tau.rotateXY(-phi  );
  nu_tau.rotateXZ(M_PI-theta);

  for(unsigned int i=0; i<tau_daughters.size();i++)
  {
    tau_daughters[i].rotateXY(-phi  );
    tau_daughters[i].rotateXZ(M_PI-theta);
  }

  //cout<<endl<<"AFTER 2: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);

  // 3) boost tau_daughters along Z to rest frame of tau

  for(unsigned int i=0; i<tau_daughters.size();i++)
    tau_daughters[i].boostAlongZ(-tau.pz(),tau.e());

  //cout<<endl<<"AFTER 3: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);

  // 4) Now rotate tau daughters second time
  //    so that nu_tau (from tau daughters list) is on Z axis

  //    We can not be sure  if tau_daughters[0] is neutrino !!!
  //    That is the case, for tauola generated samples, but not in general!
  unsigned int i_stored = 0;

  *phi2=0;
  *theta2=0;

  for(unsigned int i=0; i<tau_daughters.size();i++)
  {
    if(abs(tau_daughters[i].pdgid())==16){
     *phi2     = tau_daughters[i].getAnglePhi();

     tau_daughters[i].rotateXY( -(*phi2)   );

     *theta2   = tau_daughters[i].getAngleTheta();

     tau_daughters[i].rotateXZ( -(*theta2) );
     
     i_stored = i;
     break;
    }
  }

  for(unsigned int i=0; i<tau_daughters.size();i++)
  {
    if(i != i_stored) {
      tau_daughters[i].rotateXY( -(*phi2)   );
      tau_daughters[i].rotateXZ( -(*theta2) );
    }
  }

  //cout<<endl<<"AFTER 4: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);
}

/*******************************************************************************
  Calculates polarimetric vector HH of the tau decay. 

  HH[3] is timelike because we use FORTRAN methods to calculate HH.
  First decide what is the channel. After that, 4-vectors
  are moved to tau rest frame of tau.
  Polarimetric vector HH is then rotated using angles phi and theta.

  Order of the tau decay products does not matter (it is adjusted) 
*******************************************************************************/
// ATLAS NOTE: this function has been replaced by a newer version from 
// TauSpinner developers, which accounts for decays with kaons
double* TauSpinnerWeighter::calculateHH(int tau_pdgid, vector<Particle> &tau_daughters, double phi, double theta)
{
  int    channel = 0;
  double *HH     = new double[4];

  HH[0]=HH[1]=HH[2]=HH[3]=0.0;  

  vector<int>  pdgid;

  // Create list of tau daughters pdgid
  for(unsigned int i=0; i<tau_daughters.size(); i++)
    pdgid.push_back( tau_daughters[i].pdgid() );

  // 17.04.2014: If Tauola++ is used for generation, 
  // jaki_.ktom  may be changed to 11 at the time of storing decay to event record 
  // (case of full spin effects).
  // For Tauola++ jaki_.ktom is later of no use so 11 does not create problems.
  // For TauSpinner processing, jaki_.ktom should always be 1
  // This was the problem if Tauola++ generation and TauSpinner were used simultaneously.
  jaki_.ktom = 1;

  // tau^- --> pi^- nu_tau
  // tau^+ --> pi^+ anti_nu_tau
  // tau^- --> K^-  nu_tau
  // tau^+ --> K^+  anti_nu_tau
  if( pdgid.size()==2 &&
      (
        ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211) ) ||
        ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211) ) ||
        ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321) ) ||
        ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321) )
      )
    ) {
    channel = 3; // channel: numbering convention of TAUOLA
    if(abs(pdgid[1])==321) channel = 6;
    DEBUG( cout<<"Channel "<<channel<<"  : "; )
    //        PXQ=AMTAU*EPI
    //        PXN=AMTAU*ENU
    //        QXN=PPI(4)*PNU(4)-PPI(1)*PNU(1)-PPI(2)*PNU(2)-PPI(3)*PNU(3)

    //        BRAK=(GV**2+GA**2)*(2*PXQ*QXN-AMPI**2*PXN)
    //        HV(I)=-ISGN*2*GA*GV*AMTAU*(2*PPI(I)*QXN-PNU(I)*AMPI**2)/BRAK

    const double AMTAU = 1.777;
    // is mass of the Pi+- OR K+-
    double AMPI  = sqrt(tau_daughters[1].e() *tau_daughters[1].e()
                       -tau_daughters[1].px()*tau_daughters[1].px()
                       -tau_daughters[1].py()*tau_daughters[1].py()
                       -tau_daughters[1].pz()*tau_daughters[1].pz());

    // two-body decay is so simple, that matrix element is calculated here
    double PXQ=AMTAU*tau_daughters[1].e();
    double PXN=AMTAU*tau_daughters[0].e();
    double QXN=tau_daughters[1].e()*tau_daughters[0].e()-tau_daughters[1].px()*tau_daughters[0].px()-tau_daughters[1].py()*tau_daughters[0].py()-tau_daughters[1].pz()*tau_daughters[0].pz();
    double BRAK=(2*PXQ*QXN-AMPI*AMPI*PXN);

    WTamplit = (1.16637E-5)*(1.16637E-5)*BRAK/2.;//AMPLIT=(GFERMI)**2*BRAK/2. //WARNING: Note for normalisation Cabbibo angle is missing!
    HH[0] = AMTAU*(2*tau_daughters[1].px()*QXN-tau_daughters[0].px()*AMPI*AMPI)/BRAK;
    HH[1] = AMTAU*(2*tau_daughters[1].py()*QXN-tau_daughters[0].py()*AMPI*AMPI)/BRAK;
    HH[2] = AMTAU*(2*tau_daughters[1].pz()*QXN-tau_daughters[0].pz()*AMPI*AMPI)/BRAK;
    HH[3] = 1.0;
  }

  // tau^- --> pi^- pi^0 nu_tau
  // tau^+ --> pi^+ pi^0 anti_nu_tau
  // tau^- --> K^- K^0 nu_tau;          K^0 may be K_L K_S too.
  // tau^+ --> K^+ K^0 anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 111) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 111) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 311) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 311) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 310) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 310) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 130) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 130) )
 
           )
         ) {

    channel = 4;
    DEBUG( cout<<"Channel "<<channel<<"  : "; )
    //      PRODPQ=PT(4)*QQ(4)
    //      PRODNQ=PN(4)*QQ(4)-PN(1)*QQ(1)-PN(2)*QQ(2)-PN(3)*QQ(3)
    //      PRODPN=PT(4)*PN(4)
    //      BRAK=(GV**2+GA**2)*(2*PRODPQ*PRODNQ-PRODPN*QQ2)
    //      HV(I)=2*GV*GA*AMTAU*(2*PRODNQ*QQ(I)-QQ2*PN(I))/BRAK

    const double AMTAU = 1.777;

    int   MNUM = 0;
    if(tau_daughters[2].pdgid() != 111) { MNUM=3; channel = 22;} // sub case of decay to K-K0
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float AMPLIT = 0.0;
    float HV[4]  = { 0.0 };

    dam2pi_( &MNUM, PT, PN, PIM1, PIM2, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> K^-  pi^0 nu_tau
  // tau^+ --> K^+  pi^0 anti_nu_tau
  // tau^- --> pi^- K_S0 nu_tau
  // tau^+ --> pi^+ K_S0 anti_nu_tau
  // tau^- --> pi^- K_L0 nu_tau
  // tau^+ --> pi^+ K_L0 anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 130) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 130) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 310) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 310) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 311) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 311) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 111) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 111) )
           )
         ) {

    channel = 7;
    DEBUG( cout<<"Channel "<<channel<<"  : "; )
    //      PRODPQ=PT(4)*QQ(4)
    //      PRODNQ=PN(4)*QQ(4)-PN(1)*QQ(1)-PN(2)*QQ(2)-PN(3)*QQ(3)
    //      PRODPN=PT(4)*PN(4)
    //      BRAK=(GV**2+GA**2)*(2*PRODPQ*PRODNQ-PRODPN*QQ2)
    //      HV(I)=2*GV*GA*AMTAU*(2*PRODNQ*QQ(I)-QQ2*PN(I))/BRAK

    const double AMTAU = 1.777;

    double QQ[4];
    QQ[0]=tau_daughters[1].e() -tau_daughters[2].e() ;
    QQ[1]=tau_daughters[1].px()-tau_daughters[2].px();
    QQ[2]=tau_daughters[1].py()-tau_daughters[2].py();
    QQ[3]=tau_daughters[1].pz()-tau_daughters[2].pz();

    double PKS[4];
    PKS[0]=tau_daughters[1].e() +tau_daughters[2].e() ;
    PKS[1]=tau_daughters[1].px()+tau_daughters[2].px();
    PKS[2]=tau_daughters[1].py()+tau_daughters[2].py();
    PKS[3]=tau_daughters[1].pz()+tau_daughters[2].pz();

    // orthogonalization of QQ wr. to PKS
    double PKSD=PKS[0]*PKS[0]-PKS[1]*PKS[1]-PKS[2]*PKS[2]-PKS[3]*PKS[3];
    double QQPKS=QQ[0]*PKS[0]-QQ[1]*PKS[1]-QQ[2]*PKS[2]-QQ[3]*PKS[3];

    QQ[0]=QQ[0]-PKS[0]*QQPKS/PKSD;
    QQ[1]=QQ[1]-PKS[1]*QQPKS/PKSD;
    QQ[2]=QQ[2]-PKS[2]*QQPKS/PKSD;
    QQ[3]=QQ[3]-PKS[3]*QQPKS/PKSD;

    double PRODPQ=AMTAU*QQ[0];
    double PRODNQ=tau_daughters[0].e() *QQ[0]
                 -tau_daughters[0].px()*QQ[1]
                 -tau_daughters[0].py()*QQ[2]
                 -tau_daughters[0].pz()*QQ[3];
    double PRODPN=AMTAU*tau_daughters[0].e();
    double QQ2   =QQ[0]*QQ[0]-QQ[1]*QQ[1]-QQ[2]*QQ[2]-QQ[3]*QQ[3];

    // in this case matrix element is calculated here
    double BRAK=(2*PRODPQ*PRODNQ-PRODPN*QQ2);

    WTamplit = (1.16637E-5)*(1.16637E-5)*BRAK/2.;//AMPLIT=(GFERMI)**2*BRAK/2. WARNING: Note for normalisation Cabibbo angle is missing!
    HH[0]=AMTAU*(2*PRODNQ*QQ[1]-QQ2*tau_daughters[0].px())/BRAK;
    HH[1]=AMTAU*(2*PRODNQ*QQ[2]-QQ2*tau_daughters[0].py())/BRAK;
    HH[2]=AMTAU*(2*PRODNQ*QQ[3]-QQ2*tau_daughters[0].pz())/BRAK;
    HH[3]=1.0;
  }

  // tau^- --> e^- anti_nu_e      nu_tau
  // tau^+ --> e^+      nu_e anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 11,-12) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-11, 12) )
           )
         ) {
    DEBUG( cout<<"Channel 1  : "; )
    channel = 1;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]={0},XA[4] nu_e, QP[4] e, XN[4] neutrino tauowe, AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 0;
    double XK0DEC = 0.01;
    double XK[4] = { 0.0 };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };

    // We fix 4-momenta of electron and electron neutrino
    // Since electrons have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.511e-3*0.511e-3);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );

    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT;  // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> e^- anti_nu_e      nu_tau + gamma
  // tau^+ --> e^+      nu_e anti_nu_tau + gamma
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 11,-12, 22) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-11, 12, 22) )
           )
         ) {
    DEBUG( cout<<"Channel 1b : "; )
    channel = 1;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]  gamma, XA[4] nu_e, QP[4] e, XN[4] neutrino tau , AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 1;
    double XK0DEC = 0.01;
    double XK[4] = { tau_daughters[3].px(), tau_daughters[3].py(), tau_daughters[3].pz(), tau_daughters[3].e() };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };
    
    // We fix 4-momenta of electron and electron neutrino and photon
    // Since electrons have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.511e-3*0.511e-3);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );
    XK[3] = sqrt( XK[0]*XK[0] + XK[1]*XK[1] + XK[2]*XK[2] );
    // XK0DEC must be smaller in TauSpinner  than what was used in generation. We do not use virt. corr anyway.
    if(XK0DEC > XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]))  XK0DEC=0.5*XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]);
    
    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT; // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> mu^- antui_nu_mu      nu_tau
  // tau^+ --> mu^+       nu_mu anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 13,-14) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-13, 14) )
           )
         ) {

    DEBUG( cout<<"Channel 2  : "; )
    channel = 2;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]={0},XA[4] nu_mu, QP[4] mu, XN[4] neutrino tauowe, AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 0;
    double XK0DEC = 0.01;
    double XK[4] = { 0.0 };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };

    // We fix 4-momenta of muon and muon neutrino
    // Since muon have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.105659*0.105659);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );
    
    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT; // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> mu^- antui_nu_mu      nu_tau + gamma
  // tau^+ --> mu^+       nu_mu anti_nu_tau + gamma
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 13,-14, 22) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-13, 14, 22) )
           )
         ) {

    DEBUG( cout<<"Channel 2b : "; )
    channel = 2;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]  gamma, XA[4] nu_mu, QP[4] mu, XN[4] neutrino tau, AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 1;
    double XK0DEC = 0.01;
    double XK[4] = { tau_daughters[3].px(), tau_daughters[3].py(), tau_daughters[3].pz(), tau_daughters[3].e() };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };

    // We fix 4-momenta of muon and muon neutrino and photon
    // Since muons have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.105659*0.105659);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );
    XK[3] = sqrt( XK[0]*XK[0] + XK[1]*XK[1] + XK[2]*XK[2] );
    // XK0DEC must be smaller in TauSpinner  than what was used in generation. We do not use virt. corr anyway.
    if(XK0DEC > XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]))  XK0DEC=0.5*XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]);


    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT; // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> pi^- pi^0 pi^0 nu_tau
  // tau^+ --> pi^+ pi^0 pi^0 anti_nu_tau
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 111, 111,-211) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 111, 111, 211) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 5;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi0[4], pi0[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    int   MNUM = 0;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4]  = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=2;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> pi^+ pi^- pi^- nu_tau
  // tau^+ --> pi^- pi^+ pi^+ anti_nu_tau
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211,-211, 211) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 211,-211) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 5;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    int   MNUM = 0;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> K^+ pi^- pi^+ nu_tau    // prepared for modes with kaons
  // tau^+ --> K^- pi^- pi^+ anti_nu_tau

  // tau^- --> pi^+ K^- K^- nu_tau    // prepared for modes with kaons
  // tau^+ --> pi^- K^+ K^+ anti_nu_tau
  // tau^- --> K^+ K^- pi^- nu_tau    // prepared for modes with kaons
  // tau^+ --> K^- K^+ pi^+ anti_nu_tau

  // tau^- --> pi^- K^0 pi^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> pi^+ K^0 pi^0 anti_nu_tau


  // tau^- --> pi^- K^0 K^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> pi^+ K^0 K^0 anti_nu_tau


  // tau^- --> K^- K^0 pi^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> K^+ K^0 pi^0 anti_nu_tau

  // tau^- --> K^- pi^0 pi^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> K^+ pi^0 pi^0 anti_nu_tau

   //  3              -3,-1, 3, 0, 0, 0,    -4,-1, 4, 0, 0, 0,  
   //  4              -3, 2,-4, 0, 0, 0,     2, 2,-3, 0, 0, 0,  
   //  5              -3,-1, 1, 0, 0, 0,    -1, 4, 2, 0, 0, 0,  

  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, -211, 321) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321,  211,-321) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 14;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;

   //       IF(I.EQ.14) NAMES(I-7)='  TAU-  -->  K-, PI-,  K+      '
    int   MNUM = 1;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }



  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  311, -211, 311  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  311,  211, 311  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  311, -211, 310  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  311,  211, 310  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  311, -211, 130  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  311,  211, 130  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  310, -211, 311  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  310,  211, 311  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  310, -211, 310  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  310,  211, 310  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  310, -211, 130  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  310,  211, 130  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  130, -211, 311  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  130,  211, 311  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  130, -211, 310  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  130,  211, 310  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  130, -211, 130  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  130,  211, 130  ) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 15;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
   //       IF(I.EQ.15) NAMES(I-7)='  TAU-  -->  K0, PI-, K0B      '
    int   MNUM = 2;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }



  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, 311,  111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321, 311,  111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, 310,  111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321, 310,  111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, 130,  111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321, 130,  111) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 16;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
   //       IF(I.EQ.16) NAMES(I-7)='  TAU-  -->  K-,  K0, PI0      '
    int   MNUM = 3;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }



  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  111,  111,-321) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  111,  111, 321) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 17;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
   //       IF(I.EQ.17) NAMES(I-7)='  TAU-  --> PI0  PI0   K-      ''
    int   MNUM = 4;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }


  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321,-211, 211) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 211,-211) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 18;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    //       IF(I.EQ.18) NAMES(I-7)='  TAU-  -->  K-  PI-  PI+      '
    int   MNUM = 5;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 311, 111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 311, 111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 310, 111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 310, 111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 130, 111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 130, 111) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 19;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    //       IF(I.EQ.19) NAMES(I-7)='  TAU-  --> PI-  K0B  PI0      '
    int   MNUM = 6;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }
  // tau^- --> pi^+ pi^+ pi^0 pi^- nu_tau
  // tau^+ --> pi^- pi^- pi^0 pi^+ anti_nu_tau
  else if( pdgid.size()==5 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211,-211, 211, 111) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 211,-211, 111) )
           )
         ) {
    DEBUG( cout<<"Channel 8  : "; )
    channel = 8;

    const double AMTAU = 1.777;
    int   MNUM = 1;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIZ [4] = { (float)tau_daughters[4].px(), (float)tau_daughters[4].py(), (float)tau_daughters[4].pz(), (float)tau_daughters[4].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    dam4pi_( &MNUM, PT, PN, PIM1, PIM2, PIZ, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }
  // tau^- --> pi^0 pi^0 pi^0 pi^- nu_tau
  // tau^+ --> pi^0 pi^0 pi^0 pi^+ anti_nu_tau
  else if( pdgid.size()==5 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 111, 111, 111,-211) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 111, 111, 111, 211) )
           )
         ) {
    DEBUG( cout<<"Channel 9  : "; )
    channel = 9;

    const double AMTAU = 1.777;
    int   MNUM = 2;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIZ [4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float PIPL[4] = { (float)tau_daughters[4].px(), (float)tau_daughters[4].py(), (float)tau_daughters[4].pz(), (float)tau_daughters[4].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    dam4pi_( &MNUM, PT, PN, PIM1, PIM2, PIZ, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }
  else {

    DEBUG( cout<<tau_daughters.size()<<"-part  ???: "; )

  }

  // Now rotate vector HH using angles phi and theta
  Particle HHbuf(HH[0], HH[1], HH[2], HH[3], 0);
  
  HHbuf.rotateXZ(theta);
  HHbuf.rotateXY(phi);
  
  HH[0] = HHbuf.px();
  HH[1] = HHbuf.py();
  HH[2] = HHbuf.pz();
  HH[3] = HHbuf.e ();

  return HH;
}

/*******************************************************************************
 Returns longitudinal polarization of the single tau (in Z/gamma* -> tau+ tau- case) averaged over
 incoming configurations
 S: invariant mass^2 of the bozon
 &sp_tau: first tau
 &sp_nu_tau: second tau  (in this case it is misleading name)
 Hidden output: WTnonSM
 Hidden input:  relWTnonS, nonSM2
*******************************************************************************/
double TauSpinnerWeighter::getLongitudinalPolarization(double S, SimpleParticle &sp_tau, SimpleParticle &sp_nu_tau)
{
  // tau+ and tau- in lab frame
  Particle tau_plus (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
  Particle tau_minus( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

  // P_QQ = sum of tau+ and tau- in lab frame
  Particle P_QQ( tau_plus.px()+tau_minus.px(), tau_plus.py()+tau_minus.py(), tau_plus.pz()+tau_minus.pz(), tau_plus.e()+tau_minus.e(), 0 );
  
  Particle P_B1(0, 0, 1, 1, 0);
  Particle P_B2(0, 0,-1, 1, 0);

  tau_plus. boostToRestFrame(P_QQ);
  tau_minus.boostToRestFrame(P_QQ);
  P_B1.     boostToRestFrame(P_QQ);
  P_B2.     boostToRestFrame(P_QQ);
  
  double costheta1 = (tau_plus.px()*P_B1.px()    +tau_plus.py()*P_B1.py()    +tau_plus.pz()*P_B1.pz()    ) /
                 sqrt(tau_plus.px()*tau_plus.px()+tau_plus.py()*tau_plus.py()+tau_plus.pz()*tau_plus.pz()) /
                 sqrt(P_B1.px()    *P_B1.px()    +P_B1.py()    *P_B1.py()    +P_B1.pz()    *P_B1.pz()    );

  double costheta2 = (tau_minus.px()*P_B2.px()    +tau_minus.py()*P_B2.py()    +tau_minus.pz()*P_B2.pz()    ) /
                 sqrt(tau_minus.px()*tau_minus.px()+tau_minus.py()*tau_minus.py()+tau_minus.pz()*tau_minus.pz()) /
                 sqrt(P_B2.px()    *P_B2.px()    +P_B2.py()    *P_B2.py()    +P_B2.pz()    *P_B2.pz()    );
               
  double sintheta1 = sqrt(1-costheta1*costheta1);
  double sintheta2 = sqrt(1-costheta2*costheta2);
  
  // Cosine of hard scattering
  double costhe = (costheta1*sintheta2 + costheta2*sintheta1) / (sintheta1 + sintheta2);
  
  // Invariant mass^2 of, tau+tau- pair plus photons, system!
  double SS     = S; // other option is for tests: P_QQ.recalculated_mass()*P_QQ.recalculated_mass();
  
  /*
    We need to fix sign of costhe and attribute ID. 
    calculate x1,x2;  // x1*x2 = SS/CMSENE/CMSENE; // (x1-x2)/(x1+x2)=P_QQ/CMSENE*2 in lab; 
    calculate weight WID[]=sig(ID,SS,+/-costhe)* f(x1,+/-ID,SS) * f(x2,-/+ID,SS) ; respectively for u d c s b 
    f(x,ID,SS,CMSENE)=x*(1-x) // for the start it will invoke library
    on the basis of this generate ID and set sign for costhe. 
    then we calculate polarization averaging over incoming states.
  */
  
  double x1x2  = SS/CMSENE/CMSENE;
  double x1Mx2 = P_QQ.pz()/CMSENE*2;
  
  double x1 = (  x1Mx2 + sqrt(x1Mx2*x1Mx2 + 4*x1x2) )/2;
  double x2 = ( -x1Mx2 + sqrt(x1Mx2*x1Mx2 + 4*x1x2) )/2;
  
  double WID[11];
  WID[0] = f(x1, 0,SS,CMSENE)*f(x2, 0,SS,CMSENE) * sigborn(0,SS, costhe);
  WID[1] = f(x1, 1,SS,CMSENE)*f(x2,-1,SS,CMSENE) * sigborn(1,SS, costhe);
  WID[2] = f(x1,-1,SS,CMSENE)*f(x2, 1,SS,CMSENE) * sigborn(1,SS,-costhe);
  WID[3] = f(x1, 2,SS,CMSENE)*f(x2,-2,SS,CMSENE) * sigborn(2,SS, costhe);
  WID[4] = f(x1,-2,SS,CMSENE)*f(x2, 2,SS,CMSENE) * sigborn(2,SS,-costhe);
  WID[5] = f(x1, 3,SS,CMSENE)*f(x2,-3,SS,CMSENE) * sigborn(3,SS, costhe);
  WID[6] = f(x1,-3,SS,CMSENE)*f(x2, 3,SS,CMSENE) * sigborn(3,SS,-costhe);
  WID[7] = f(x1, 4,SS,CMSENE)*f(x2,-4,SS,CMSENE) * sigborn(4,SS, costhe);
  WID[8] = f(x1,-4,SS,CMSENE)*f(x2, 4,SS,CMSENE) * sigborn(4,SS,-costhe);
  WID[9] = f(x1, 5,SS,CMSENE)*f(x2,-5,SS,CMSENE) * sigborn(5,SS, costhe);
  WID[10]= f(x1,-5,SS,CMSENE)*f(x2, 5,SS,CMSENE) * sigborn(5,SS,-costhe);
  
  double sum = 0.0;  // normalize
  for(int i=0;i<=10;i++) sum+=WID[i];
  
  if( sum == 0.0 )
  {
    cout << "Tauspinner::calculateWeightFromParticlesH WARNING: sum of WID[0]-WID[10] is 0. Check LHAPDF configuration" << endl;
  }

  WTnonSM=1.0;
  if(relWTnonSM==0)  WTnonSM=sum;
  if(nonSM2==1)
  {
    double WID2[11];
    WID2[0] = f(x1, 0,SS,CMSENE)*f(x2, 0,SS,CMSENE) * sigborn(0,SS, costhe) * plweight(0,SS, costhe); // plweight = ratio of cross-section nonSM/SM
    WID2[1] = f(x1, 1,SS,CMSENE)*f(x2,-1,SS,CMSENE) * sigborn(1,SS, costhe) * plweight(1,SS, costhe);
    WID2[2] = f(x1,-1,SS,CMSENE)*f(x2, 1,SS,CMSENE) * sigborn(1,SS,-costhe) * plweight(1,SS,-costhe);
    WID2[3] = f(x1, 2,SS,CMSENE)*f(x2,-2,SS,CMSENE) * sigborn(2,SS, costhe) * plweight(2,SS, costhe);
    WID2[4] = f(x1,-2,SS,CMSENE)*f(x2, 2,SS,CMSENE) * sigborn(2,SS,-costhe) * plweight(2,SS,-costhe);
    WID2[5] = f(x1, 3,SS,CMSENE)*f(x2,-3,SS,CMSENE) * sigborn(3,SS, costhe) * plweight(3,SS, costhe);
    WID2[6] = f(x1,-3,SS,CMSENE)*f(x2, 3,SS,CMSENE) * sigborn(3,SS,-costhe) * plweight(3,SS,-costhe);
    WID2[7] = f(x1, 4,SS,CMSENE)*f(x2,-4,SS,CMSENE) * sigborn(4,SS, costhe) * plweight(4,SS, costhe);
    WID2[8] = f(x1,-4,SS,CMSENE)*f(x2, 4,SS,CMSENE) * sigborn(4,SS,-costhe) * plweight(4,SS,-costhe);
    WID2[9] = f(x1, 5,SS,CMSENE)*f(x2,-5,SS,CMSENE) * sigborn(5,SS, costhe) * plweight(5,SS, costhe);
    WID2[10]= f(x1,-5,SS,CMSENE)*f(x2, 5,SS,CMSENE) * sigborn(5,SS,-costhe) * plweight(5,SS,-costhe);
    
    double sum2 = 0.0;  // normalize
    for(int i=0;i<=10;i++) sum2+=WID2[i];

    WTnonSM=sum2/sum ; 
    if(relWTnonSM==0)  WTnonSM=sum2;
    }
  
  double pol = 0.0;
  //  double Rxx = 0.0;
  //  double Ryy = 0.0;
  
  if(IfHiggs && nonSM2==1) {   // we assume that only glue glue process contributes for Higgs
    double polp = plzap2(0,15,S,costhe);  // 0 means incoming gluon, 15 means outgoing tau
    pol += (2*(1-polp)-1);
     return pol;
  }
  if(IfHiggs) return NAN;

  // case of Z/gamma 
  for(int i=0;i<=10;i++) WID[i]/=sum;


  R11 = 0.0;
  R22 = 0.0;

  int ICC = -1;
  
  for(int i=1;i<=10;i++)
  {

    ICC = i;
    double cost = costhe;
    // first beam quark or antiquark

    if( ICC==2 || ICC==4 || ICC==6 || ICC==8 || ICC==10 )  cost = -cost;

    // ID of incoming quark (up or down type)
    int                     ID = 2;          
    if( ICC==7 || ICC==8  ) ID = 4;
    if( ICC==1 || ICC==2  ) ID = 1;
    if( ICC==5 || ICC==6  ) ID = 3;
    if( ICC==9 || ICC==10 ) ID = 5;

    int tau_pdgid = 15;

    double polp = plzap2(ID,tau_pdgid,S,cost);
    pol += (2*(1-polp)-1)*WID[i];

    // we obtain transverse spin components of density matrix from Tauolapp pre-tabulated O(alpha) EW results
    //  
    Tauolapp::TauolaParticlePair pp;

    // Set them to 0 in case no tables are loaded by Tauola++
    pp.m_R[1][1] = pp.m_R[2][2] = 0.0;

    pp.recalculateRij(ID,tau_pdgid,S,cost);

    // These calculations are valid for Standard Model only
    R11 += WID[i]*pp.m_R[1][1];
    R22 += WID[i]*pp.m_R[2][2];
  }
  
  // Calculations are prepared only for pp collision.
  // Otherwise pol = 0.0
  if(!Ipp) pol=0.0;

  return pol;
}

/*******************************************************************************
  Check if pdg's of particles in the vector<Particle>&particles  match the 
  of (p1,p2,p3,p4,p5,p6)

  Returns true if 'particles' contain all of the listed pdgid-s.
  If it does - vector<Particle>&particles will be sorted in the order  
  as listed pdgid-s.

  It is done so the order of particles is the same as the order used by
  TAUOLA Fortran routines.
*******************************************************************************/
bool TauSpinnerWeighter::channelMatch(vector<Particle> &particles, int p1, int p2, int p3, int p4, int p5, int p6)
{
  // Copy pdgid-s of all particles
  vector<int> list;
  
  for(unsigned int i=0;i<particles.size();i++) list.push_back(particles[i].pdgid());
  
  // Create array out of pdgid-s
  int p[6] = { p1, p2, p3, p4, p5, p6 };

  // 1) Check if 'particles' contain all pdgid-s on the list 'p'
  
  for(int i=0;i<6;i++)
  {
    // if the pdgid is zero - finish
    if(p[i]==0) break;
    
    bool found = false;
    
    for(unsigned int j=0;j<list.size(); j++)
    {
      // if pdgid is found - erese it from the list and search for the next one
      if(list[j]==p[i])
      {
        found = true;
        list.erase(list.begin()+j);
        break;
      }
    }
    
    if(!found) return false;
  }
  
  // if there are more particles on the list - there is no match
  if(list.size()!=0) return false;

  
  // 2) Rearrange particles to match the order of pdgid-s listed in array 'p'

  vector<Particle> newList;
  
  for(int i=0;i<6;i++)
  {
    // if the pdgid is zero - finish
    if(p[i]==0) break;
    
    for(unsigned int j=0;j<particles.size(); j++)
    {
      // if pdgid is found - copy it to new list and erese from the old one
      if(particles[j].pdgid()==p[i])
      {
        newList.push_back(particles[j]);
        particles.erase(particles.begin()+j);
        break;
      }
    }
  }
  
  particles = newList;

  return true;
}

/*******************************************************************************
 Prints out two vertices:
   W   -> tau, nu_tau
   tau -> tau_daughters
*******************************************************************************/
void TauSpinnerWeighter::print(Particle &W, Particle &nu_tau, Particle &tau, vector<Particle> &tau_daughters) {

  nu_tau.print();
  tau   .print();

  double px=nu_tau.px()+tau.px();
  double py=nu_tau.py()+tau.py();
  double pz=nu_tau.pz()+tau.pz();
  double e =nu_tau.e ()+tau.e ();

  // Print out  sum of tau and nu_tau  and also  W  momentum for comparison
  cout<<"--------------------------------------------------------------------------------------------------------"<<endl;
  Particle sum1(px,py,pz,e,0);
  sum1.print();
  W   .print();

  cout<<endl;

  // Print out tau daughters
  for(unsigned int i=0; i<tau_daughters.size();i++) tau_daughters[i].print();

  // Print out sum of tau decay products, and also tau momentum for comparison
  cout<<"--------------------------------------------------------------------------------------------------------"<<endl;
  Particle *sum2 = vector_sum(tau_daughters);
  sum2->print();
  tau.print();
  cout<<endl;
  
  delete sum2;
}

/*******************************************************************************
 Sums all 4-vectors of the particles on the list
*******************************************************************************/
Particle *TauSpinnerWeighter::vector_sum(vector<Particle> &x) {

  double px=0.0,py=0.0,pz=0.0,e=0.0;

  for(unsigned int i=0; i<x.size();i++)
  {
    px+=x[i].px();
    py+=x[i].py();
    pz+=x[i].pz();
    e +=x[i].e();
  }

  Particle *sum = new Particle(px,py,pz,e,0);
  return sum;
}



// ATLAS: functions from nonSM.cxx

double TauSpinnerWeighter::nonSM_born(int ID, double S, double cost, int H1, int H2, int key)
{
  if(IfHiggs) return nonSM_bornH(ID,S,cost,H1,H2,key);
  else        return nonSM_bornZ(ID,S,cost,H1,H2,key);
}


double TauSpinnerWeighter::default_nonSM_born(int ID, double S, double cost, int H1, int H2, int key)
{
  cout<<"TauSpinner::default_nonSM_born: this function is dummy\n"<<endl;
  cout<<"             user must provide his own nonSM_born"<<endl;
  cout<<"             see: nonSM_adopt() and set_nonSM_born( NULL )"<<endl;
  cout<<"             in TauSpinner/examples/tau-reweight-test.cxx for details"<<endl;
  exit(-1);
  return 1.0;
}


double TauSpinnerWeighter::default_nonSM_bornH(int ID, double S, double cost, int H1, int H2, int key)
{
  cout<<"TauSpinner::default_nonSM_born: this function is dummy\n"<<endl;
  cout<<"             user must provide his own nonSM_born"<<endl;
  cout<<"             see: nonSM_adopt() and set_nonSM_born( NULL )"<<endl;
  cout<<"             in TauSpinner/examples/tau-reweight-test.cxx for details"<<endl;
  exit(-1);
  return 1.0;
}

// void set_nonSM_born( double (*fun)(int, double, double, int, int, int) )
void TauSpinnerWeighter::set_nonSM_born( std::function<double(int, double, double, int, int, int)> fun)
{
  if(fun==NULL)
    nonSM_bornZ = std::bind(&TauSpinnerWeighter::default_nonSM_born,
                            this,
                            std::placeholders::_1,
                            std::placeholders::_2,
                            std::placeholders::_3,
                            std::placeholders::_4,
                            std::placeholders::_5,
                            std::placeholders::_6);
  else
    nonSM_bornZ = fun;
}

// void set_nonSM_bornH( double (*fun)(int, double, double, int, int, int) )
void TauSpinnerWeighter::set_nonSM_bornH( std::function<double(int, double, double, int, int, int)> fun)
{
  if(fun==NULL)
    nonSM_bornH = std::bind(&TauSpinnerWeighter::default_nonSM_born,
                            this,
                            std::placeholders::_1,
                            std::placeholders::_2,
                            std::placeholders::_3,
                            std::placeholders::_4,
                            std::placeholders::_5,
                            std::placeholders::_6);
  else
    nonSM_bornH = fun;
}







/*******************************************************************************
 Probability of the helicity state 
*******************************************************************************/
double TauSpinnerWeighter::plzap2(int ide, int idf, double svar, double costhe)
{
  if(ide!=0)
  {
    if(idf > 0) initwk_(&ide,&idf,&svar);
    else
    {
      int mide=-ide;
      int midf=-idf;

      initwk_(&mide,&midf,&svar);
    }
  }

  int    zero =  0;
  double one  =  1.0;
  double mone = -1.0;
  double ret  =  0.0;

  if(nonSM2==0)
  {
    ret =  t_born_(&zero,&svar,&costhe, &one, &one)
         /(t_born_(&zero,&svar,&costhe, &one, &one)
          +t_born_(&zero,&svar,&costhe,&mone,&mone));
  }
  else if(nonSM2==1)
  {
    ret =  nonSM_born(ide,svar,costhe, 1, 1, nonSM2)
         /(nonSM_born(ide,svar,costhe, 1, 1, nonSM2)
          +nonSM_born(ide,svar,costhe,-1,-1, nonSM2));

  // test of user prepared born cross section can be prepared here. 
  // convention between t_born and nonSM_born in choice of flavours
  // sign of costhe helicity signs etc have to be performed using SM version
  // of nonSM_born. Matching (up to may be overall s-dependent factor) between
  // t_born and nonSM_born must be achieved, see Section 4 for details
  // on technical tests. 
  DEBUG(
    double sm=  t_born_(&zero,&svar,&costhe, &one, &one)
              /(t_born_(&zero,&svar,&costhe, &one, &one)
               +t_born_(&zero,&svar,&costhe,&mone,&mone));
    double nsm= nonSM_born(ide,svar,costhe, 1, 1, nonSM2)
              /(nonSM_born(ide,svar,costhe, 1, 1, nonSM2)
               +nonSM_born(ide,svar,costhe,-1,-1, nonSM2));
    double smn= nonSM_born(ide,svar,costhe, 1, 1, 0     )
              /(nonSM_born(ide,svar,costhe, 1, 1, 0     )
               +nonSM_born(ide,svar,costhe,-1,-1, 0     ));

    cout<<"test of nonSM Born nonsm2="<<nonSM2 << endl;
    cout<<"ide,svar,costhe="<<ide <<" " << svar <<" "  << costhe << endl;
    cout<<"sm="<<sm <<" sm (new)="<<smn <<" nsm="<<nsm << endl;
    cout<<"sm and sm (new) should be essentially equal" << endl << endl;
    if (IfHiggs) cout << "(for Higgs we need to improve algorithm)" << endl;

  )

  }
  return ret;
}

double TauSpinnerWeighter::plweight(int ide, double svar, double costhe)
{
  if(nonSM2==0) return 1.0;
  if (ide==0 && !IfHiggs) return 1.0;

  double ret      =( nonSM_born(ide,svar,costhe, 1, 1, nonSM2)/svar
                    +nonSM_born(ide,svar,costhe,-1,-1, nonSM2)/svar)
                  /( nonSM_born(ide,svar,costhe, 1, 1, 0)/svar
		    +nonSM_born(ide,svar,costhe,-1,-1, 0)/svar);   // svar is introduced for future use
  // another option angular dependence only. Effect on x-section removed
  if(nonSMN==1) ret = ret * plnorm(ide,svar);

  return ret;
}

double TauSpinnerWeighter::plnorm(int ide, double svar)
{
  if(nonSMN==0) return 1.0;

  double c1 = 1.0/sqrt(3.0);
  double c2 = sqrt(2.0/3.0);

  double alpha  = 2*nonSM_born(ide,svar,0.0, 1, 1,0)+
                  2*nonSM_born(ide,svar,0.0,-1,-1,0);
  double beta   =   nonSM_born(ide,svar, c1, 1, 1,0) + nonSM_born(ide,svar,-c1, 1, 1,0)+
                    nonSM_born(ide,svar, c1,-1,-1,0) + nonSM_born(ide,svar,-c1,-1,-1,0);
  double gamma  =   nonSM_born(ide,svar, c2, 1, 1,0) + nonSM_born(ide,svar,-c2, 1, 1,0)+
                    nonSM_born(ide,svar, c2,-1,-1,0) + nonSM_born(ide,svar,-c2,-1,-1,0);
  double ret = ( alpha + 0.9*(gamma+alpha-2*beta) + 0.5*(4*beta-3*alpha-gamma) );

  alpha  = 2*nonSM_born(ide,svar,0.0, 1, 1,nonSM2)+
           2*nonSM_born(ide,svar,0.0,-1,-1,nonSM2);
  beta   =   nonSM_born(ide,svar, c1, 1, 1,nonSM2) + nonSM_born(ide,svar,-c1, 1, 1,nonSM2)+
             nonSM_born(ide,svar, c1,-1,-1,nonSM2) + nonSM_born(ide,svar,-c1,-1,-1,nonSM2);
  gamma  =   nonSM_born(ide,svar, c2, 1, 1,nonSM2) + nonSM_born(ide,svar,-c2, 1, 1,nonSM2)+
             nonSM_born(ide,svar, c2,-1,-1,nonSM2) + nonSM_born(ide,svar,-c2,-1,-1,nonSM2);

  ret = ret / ( alpha + 0.9*(gamma+alpha-2*beta) + 0.5*(4*beta-3*alpha-gamma) );

  return ret;
}

void TauSpinnerWeighter::nonSMHcorrPol(double S, SimpleParticle &tau1, SimpleParticle &tau2,
                   double *corrX2, double *polX2)
{  // tau+ and tau- in lab frame
  Particle tau_plus (    tau1.px(),    tau1.py(),    tau1.pz(),    tau1.e(),    tau1.pdgid() );
  Particle tau_minus(    tau2.px(),    tau2.py(),    tau2.pz(),    tau2.e(),    tau2.pdgid() );

  // P_QQ = sum of tau+ and tau- in lab frame
  Particle P_QQ( tau_plus.px()+tau_minus.px(), tau_plus.py()+tau_minus.py(), tau_plus.pz()+tau_minus.pz(), tau_plus.e()+tau_minus.e(), 0 );
  
  Particle P_B1(0, 0, 1, 1, 0);
  Particle P_B2(0, 0,-1, 1, 0);

  tau_plus. boostToRestFrame(P_QQ);
  tau_minus.boostToRestFrame(P_QQ);
  P_B1.     boostToRestFrame(P_QQ);
  P_B2.     boostToRestFrame(P_QQ);
  
  double costheta1 = (tau_plus.px()*P_B1.px()    +tau_plus.py()*P_B1.py()    +tau_plus.pz()*P_B1.pz()    ) /
                 sqrt(tau_plus.px()*tau_plus.px()+tau_plus.py()*tau_plus.py()+tau_plus.pz()*tau_plus.pz()) /
                 sqrt(P_B1.px()    *P_B1.px()    +P_B1.py()    *P_B1.py()    +P_B1.pz()    *P_B1.pz()    );

  double costheta2 = (tau_minus.px()*P_B2.px()    +tau_minus.py()*P_B2.py()    +tau_minus.pz()*P_B2.pz()    ) /
                 sqrt(tau_minus.px()*tau_minus.px()+tau_minus.py()*tau_minus.py()+tau_minus.pz()*tau_minus.pz()) /
                 sqrt(P_B2.px()    *P_B2.px()    +P_B2.py()    *P_B2.py()    +P_B2.pz()    *P_B2.pz()    );
               
  double sintheta1 = sqrt(1-costheta1*costheta1);
  double sintheta2 = sqrt(1-costheta2*costheta2);
  
  // Cosine of hard scattering
  double costhe = (costheta1*sintheta2 + costheta2*sintheta1) / (sintheta1 + sintheta2);
  
  // Invariant mass of tau+tau- pair
  double SS     = S; // other option is for tests: P_QQ.recalculated_mass()*P_QQ.recalculated_mass();
  

  // corrX2 calculation
  // polX2 calculation
  // WTnonSM calculation
  *corrX2 = -1.0;  // default
  *polX2  =  0.0;  // default
  WTnonSM =  1.0;  // default
}



// ATLAS: undeclared functions from VBF/vbfdistr.cxx 


/** @brief Set vbfdistrModif function */
//  void set_alphasModif(void (*function)(double, int, int) )
  void TauSpinnerWeighter::set_alphasModif(std::function<void(double, int, int)> function )
{
    alphasModif = function;
}

/** @brief Set vbfdistrModif function */
//void set_vbfdistrModif(double (*function)(int, int, int, int, int, int, double[6][4], int, double) )
void TauSpinnerWeighter::set_vbfdistrModif(std::function<double(int, int, int, int, int, int, double[6][4], int, double) > function)
{
    vbfdistrModif = function;
}

void TauSpinnerWeighter::setPDFOpt(int QCDdefault,int QCDvariant){
  _QCDdefault=QCDdefault;
  _QCDvariant=QCDvariant;
}

  int TauSpinnerWeighter::getPDFOpt(int KEY){
    if (KEY==0 || KEY==1) return _QCDdefault;
    else return _QCDvariant;
}

  void  TauSpinnerWeighter::alphas(double Q2,int scalePDFOpt, int KEY){
    if (alphasModif) 
      { alphasModif(Q2,scalePDFOpt,KEY);
      }
    else
      {
      // any textbook LL calculation
      const double PI=3.14159265358979324;
      // number of flavors
      const double Nf = 5;
      // alphas_s at mZ
      const double AlphasMZ = 0.118;
      const double MZ =91.1876;
      // alphas_s at scale Q2 (GeV^2)
      double alfas=AlphasMZ / ( 1 + AlphasMZ/(4*PI) * (11 - 2./3 * Nf) * log(Q2/(MZ*MZ)));
      // test Alphas ( Q2 = 1000^2 ) = 0.0877445
      if(scalePDFOpt==0) alfas = 0.118;
      if(params_r_.as != alfas){// we pass alpha_s to calculation of amplitudes
	  params_r_.as = alfas;
	  //  reinitialize constants couplings for amplitude calculations
	  vbf_reinit_(&KEY);
      }
      }
}

/** Wrapper to VBDISTR and place for interface to user provided modification*/
double TauSpinnerWeighter::vbfdistr(int I1, int I2, int I3, int I4, int H1, int H2, double P[6][4], int KEY)
{
  double P_copy[6][4];
  double original_result = 0.;
  
  memcpy(P_copy,P,sizeof(double)*6*4);

  if(  KEY<2) {
    // SM mode
    return  vbfdistr_(&I1, &I2, &I3, &I4, &H1, &H2, P_copy, &KEY); 
  }
  else if(  !vbfdistrModif) {
    printf("TauSpinner::vbfdistr: User function  vbfdistrModif not declared. Setting WT_contrib = 0.0 Failed attempt with KEY = %i5. \n",KEY);
    return original_result;
  }
  else if(  KEY<4) {
    // modification mode
    int KEY_BUF=KEY-2;
    original_result=  vbfdistr_(&I1, &I2, &I3, &I4, &H1, &H2, P_copy, &KEY_BUF);
    return vbfdistrModif(I1,I2,I3,I4,H1,H2,P_copy,KEY,original_result);
  }
  else {
    // replacement mode
    return vbfdistrModif(I1,I2,I3,I4,H1,H2,P_copy,KEY,original_result);
  }
}


/** Get VBF ME2
    Returns array W[2][2] */
//---------------------------------------------------------------------------------------------------------------------------------------------------
void TauSpinnerWeighter::getME2VBF(SimpleParticle &p3i, SimpleParticle &p4i, SimpleParticle &sp_X,SimpleParticle &tau1i, SimpleParticle &tau2i, double (&W)[2][2], int KEY)
{
  
  // this may be necessary because of matrix element calculation may require absolute energy-momentum conservation!
  // FSR photons may need to be treated explicitely or with interpolation procedures.
  Particle p3(p3i.px(),p3i.py(),p3i.pz(),p3i.e(),0);
  Particle p4(p4i.px(),p4i.py(),p4i.pz(),p4i.e(),0);
  double   mtaul=sqrt(tau1i.e()*tau1i.e()-tau1i.px()*tau1i.px()-tau1i.py()*tau1i.py()-tau1i.pz()*tau1i.pz());
  Particle tau1(tau1i.px(),tau1i.py(),tau1i.pz(),tau1i.e(),0);
  Particle tau2(tau2i.px(),tau2i.py(),tau2i.pz(),tau2i.e(),0);

  // TEST begin 
  /*
  if(KEY>1){  // this is special arrangement to used alternative calculation to introduce effect of smearing on tau lepton pair (hidden in sp_X) due to showering
              // in this way we get weights for corelated configs; with and without the shower-kick.
    Particle P_tautau(tau1.px()+tau2.px(),tau1.py()+tau2.py(),tau1.pz()+tau2.pz(),tau1.e()+tau2.e(),0);
    tau1.boostToRestFrame(P_tautau);
    tau2.boostToRestFrame(P_tautau);
    p3.boostToRestFrame(P_tautau);
    p4.boostToRestFrame(P_tautau);
    //v1.boostToRestFrame(P_tautau);
    //v2.boostToRestFrame(P_tautau);
    
    Particle P_X(sp_X.px(), sp_X.py(), sp_X.pz(), sp_X.e(),0);
    tau1.boostFromRestFrame(P_X);
    tau2.boostFromRestFrame(P_X);
    p3.boostFromRestFrame(P_X);
    p4.boostFromRestFrame(P_X);
    //v1.boostFromRestFrame(P_X);
    //v2.boostFromRestFrame(P_X);    
  }
  */
  // TEST end

  // we may want to force p3,p4 to be masless.
  Particle v1(0.,0., 1.,1.,0.);
  Particle v2(0.,0.,-1.,1.,0.);

  Particle P_QQ( p3.px()+p4.px()+tau1.px()+tau2.px()+1e-8*p3.pz(),
                 p3.py()+p4.py()+tau1.py()+tau2.py()-2e-8*p3.pz(),
                 p3.pz()+p4.pz()+tau1.pz()+tau2.pz(),
                 p3.e() +p4.e() +tau1.e() +tau2.e(), 0 );
  tau1.boostToRestFrame(P_QQ);
  tau2.boostToRestFrame(P_QQ);
  p3.boostToRestFrame(P_QQ);
  p4.boostToRestFrame(P_QQ);
  v1.boostToRestFrame(P_QQ);
  v2.boostToRestFrame(P_QQ);

  // Now we can define vers1 vers2
  double xn1=sqrt(v1.px()*v1.px()+v1.py()*v1.py()+v1.pz()*v1.pz());
  double xn2=sqrt(v2.px()*v2.px()+v2.py()*v2.py()+v2.pz()*v2.pz());
  double xn12=sqrt( (v1.px()-v2.px())*(v1.px()-v2.px()) +(v1.py()-v2.py())*(v1.py()-v2.py())+(v1.pz()-v2.pz())*(v1.pz()-v2.pz()));

  double SS = P_QQ.recalculated_mass()*P_QQ.recalculated_mass(); 
  
  double x1x2  = SS/CMSENE/CMSENE;
  double x1Mx2 = P_QQ.pz()/CMSENE*2;
  
  double x1 = (  x1Mx2 + sqrt(x1Mx2*x1Mx2 + 4*x1x2) )/2;
  double x2 = ( -x1Mx2 + sqrt(x1Mx2*x1Mx2 + 4*x1x2) )/2;
 
  //---------------------------------------------------------------------------
  // Construct the matrix for FORTRAN function
  // NOTE: different order of indices than in FORTRAN!
  // four options for partonic beams.
  double P[6][4] = { { sqrt(SS)/2,   sqrt(SS)/2/xn12*(v1.px()-v2.px()),    sqrt(SS)/2/xn12*(v1.py()-v2.py()),   sqrt(SS)/2/xn12*(v1.pz()-v2.pz())  },
		     { sqrt(SS)/2,  -sqrt(SS)/2/xn12*(v1.px()-v2.px()),   -sqrt(SS)/2/xn12*(v1.py()-v2.py()),  -sqrt(SS)/2/xn12*(v1.pz()-v2.pz())  },
		     // double P[6][4] = { { sqrt(SS)/2,  -sqrt(SS)/2/xn1*v2.px(),   -sqrt(SS)/2/xn1*v2.py(),  -sqrt(SS)/2/xn1*v2.pz()  },
		     //                    { sqrt(SS)/2,   sqrt(SS)/2/xn1*v2.px(),    sqrt(SS)/2/xn1*v2.py(),   sqrt(SS)/2/xn1*v2.pz()  },
		     // double P[6][4] = { { sqrt(SS)/2,   sqrt(SS)/2/xn1*v1.px(),    sqrt(SS)/2/xn1*v1.py(),   sqrt(SS)/2/xn1*v1.pz()  },
		     //                    { sqrt(SS)/2,  -sqrt(SS)/2/xn1*v1.px(),   -sqrt(SS)/2/xn1*v1.py(),  -sqrt(SS)/2/xn1*v1.pz()  },
		     // double P[6][4] = { { sqrt(SS)/2,                      0.0,                       0.0,              sqrt(SS)/2   },
		     //                    { sqrt(SS)/2,                      0.0,                       0.0,             -sqrt(SS)/2   }, 
                     { p3.e(),      p3.px(),   p3.py(),      p3.pz()      }, 
                     { p4.e(),      p4.px(),   p4.py(),      p4.pz()      },
                     { tau1.e(),    tau1.px(), tau1.py(),    tau1.pz()    },
                     { tau2.e(),    tau2.px(), tau2.py(),    tau2.pz()    } };
  
  //
  // Calculate 'f' function for all x1 and all ID1, ID2
  //
  double  f_x1_ARRAY[11] = { 0.0 };
  double  f_x2_ARRAY[11] = { 0.0 };
  double *f_x1 = f_x1_ARRAY+5;     // This way f_x1[i],f_x2[i] can be used with 
  double *f_x2 = f_x2_ARRAY+5;     // i going from -5 to 5

  Particle P_tautau( tau1.px()+tau2.px(),  tau1.py()+tau2.py(), tau1.pz()+tau2.pz(), tau1.e()+tau2.e(),0  );

  double Q2  =  P_tautau.recalculated_mass()*P_tautau.recalculated_mass();
  double QQ2 =  P_QQ.recalculated_mass()*P_QQ.recalculated_mass();
  double PT2 =  P_QQ.px() * P_QQ.px() +  P_QQ.py()* P_QQ.py(); 

  double sumET =   p3.e()*sqrt( p3.px()*p3.px()+p3.py()*p3.py())/sqrt( p3.px()*p3.px()+p3.py()*p3.py()+p3.pz()*p3.pz())
                 + p4.e()*sqrt( p4.px()*p4.px()+p4.py()*p4.py())/sqrt( p4.px()*p4.px()+p4.py()*p4.py()+p4.pz()*p4.pz())
                 + tau1.e()*sqrt( tau1.px()*tau1.px()+tau1.py()*tau1.py())/sqrt( tau1.px()*tau1.px()+tau1.py()*tau1.py()+tau1.pz()*tau1.pz() )
                 + tau2.e()*sqrt( tau2.px()*tau2.px()+tau2.py()*tau2.py())/sqrt( tau2.px()*tau2.px()+tau2.py()*tau2.py()+tau2.pz()*tau2.pz() ) ;

  double sumMT =    sqrt( p3.recalculated_mass()*p3.recalculated_mass() + p3.px()*p3.px()+p3.py()*p3.py() )
                 +  sqrt( p4.recalculated_mass()*p4.recalculated_mass() + p4.px()*p4.px()+p4.py()*p4.py() )
                 +  sqrt( tau1.recalculated_mass()*tau1.recalculated_mass() + tau1.px()*tau1.px()+tau1.py()*tau1.py() )
                 +  sqrt( tau2.recalculated_mass()*tau2.recalculated_mass() + tau2.px()*tau2.px()+tau2.py()*tau2.py() );

  double fixed_scale = 200.;
  int scalePDFOpt = 1;  // it has to be connected to initialization and flipper
  scalePDFOpt=getPDFOpt(KEY);
  double Q2pdf = fixed_scale*fixed_scale;
  if(scalePDFOpt == 0){ 
    //
    alphas(Q2pdf,scalePDFOpt,KEY);
  } else if (scalePDFOpt == 1) {
    Q2pdf = QQ2;
    alphas(Q2pdf,scalePDFOpt,KEY);
  } else if (scalePDFOpt == 2) {
    Q2pdf = sumMT*sumMT;
    alphas(Q2pdf,scalePDFOpt,KEY);
  } else if (scalePDFOpt == 3) {
    Q2pdf = sumET*sumET;
    alphas(Q2pdf,scalePDFOpt,KEY);
  } else if (scalePDFOpt == 4) {
    Q2pdf = Q2;
    alphas(Q2pdf,scalePDFOpt,KEY);
  }

  for(int i=-5;i<=5;++i) {
    f_x1[i] = f(x1,i,Q2pdf,CMSENE);
    f_x2[i] = f(x2,i,Q2pdf,CMSENE);
  }
  // reset to zero
  W[0][0]=0.0;
  W[0][1]=0.0;
  W[1][0]=0.0;
  W[1][1]=0.0;

  if( DEBUGVBF ){
    // here you can overwrite kinematical configurations for tests
    std::cout << " " <<  std::endl;
    std::cout << "---podstawiamy 4-pedy na jakies ---" <<  std::endl;
    std::cout << "ERW: ----------------------------- " << std::endl;
  double P1[6][4]  = {{ 0.5000000E+03,  0.0,            0.0,            0.5000000E+03   },
                      { 0.5000000E+03,  0.0,            0.0,           -0.5000000E+03   }, 
                      { 0.8855009E+02, -0.2210038E+02,  0.4007979E+02, -0.7580437E+02   }, 
                      { 0.3283248E+03, -0.1038482E+03, -0.3019295E+03,  0.7649385E+02   },
                      { 0.1523663E+03, -0.1058795E+03, -0.9770827E+02,  0.4954769E+02   },
                      { 0.4307588E+03,  0.2318280E+03,  0.3595580E+03, -0.5023717E+02   } };
   std::cout << "ERW: ----------------------------- " << std::endl;
  double P2[6][4]  = {{ 0.5000000E+03,  0.0,            0.0,            0.5000000E+03   },
                      { 0.5000000E+03,  0.0,            0.0,           -0.5000000E+03   }, 
                      { 0.1177462E+03,  -0.6070512E+02,   0.7123011E+02,   0.7145150E+02   }, 
                      { 0.3509495E+03,  -0.3178775E+02,   0.8393832E+02,   0.3392779E+03   },
                      { 0.3493321E+03,   0.1840069E+03,  -0.5152712E+02,  -0.2924315E+03   },
                      { 0.1819722E+03,  -0.9151401E+02,  -0.1036413E+03,  -0.1182978E+03   } };

  std::cout << "ERW: ----------------------------- " << std::endl;
  double P3[6][4]  = {{ 0.5000000E+03,  0.0,            0.0,            0.5000000E+03   },
                      { 0.5000000E+03,  0.0,            0.0,           -0.5000000E+03   }, 
                      { 0.2586900E+03,   0.1324670E+03,  -0.1696171E+03,  -0.1435378E+03   }, 
                      { 0.1084567E+03,  -0.5735712E+02,  -0.2162482E+02,  -0.8947281E+02   },
                      { 0.4005742E+03,  -0.1580760E+03,   0.3563160E+03,   0.9223569E+02   },
                      { 0.2322791E+03,   0.8296613E+02,  -0.1650741E+03,   0.1407749E+03   } };
   std::cout << "ERW: ----------------------------- " << std::endl;
  double P4[6][4]  = {{ 0.5000000E+03,  0.0,            0.0,            0.5000000E+03   },
                      { 0.5000000E+03,  0.0,            0.0,           -0.5000000E+03   }, 
                      { 0.1595700E+03,  -0.6917808E+02,  -0.1395175E+03,  -0.3481123E+02   }, 
                      { 0.2247758E+03,  -0.1360140E+03,   0.1650340E+03,  -0.6919641E+02   },
                      { 0.2508802E+03,   0.1447863E+01,   0.2499830E+03,  -0.2107335E+02   },
                      { 0.3647740E+03,   0.2037442E+03,  -0.2754995E+03,   0.1250810E+03   } };

  for (int I1=0;I1<=5;I1++){for (int I2=0;I2<=3;I2++){
      P[I1][I2]=P1[I1][I2];
    }}

    printf("  our event     :            P[0,i]=   %16.10f  %16.10f  %16.10f  %16.10f  \n",P[0][0],P[0][1],P[0][2],P[0][3]);
    int II=1;
    printf("                             P[1,i]=   %16.10f  %16.10f  %16.10f  %16.10f  \n",P[II][0],P[II][1],P[II][2],P[II][3]);
    II=2;
    printf("                             P[2,i]=   %16.10f  %16.10f  %16.10f  %16.10f  \n",P[II][0],P[II][1],P[II][2],P[II][3]);
    II=3;
    printf("                             P[3,i]=   %16.10f  %16.10f  %16.10f  %16.10f  \n",P[II][0],P[II][1],P[II][2],P[II][3]);
    II=4;
    printf("                             P[4,i]=   %16.10f  %16.10f  %16.10f  %16.10f  \n",P[II][0],P[II][1],P[II][2],P[II][3]);
    II=5;
    printf("                             P[4,i]=   %16.10f  %16.10f  %16.10f  %16.10f  \n",P[II][0],P[II][1],P[II][2],P[II][3]);
    printf(" ===========================\n");
//#################################################################
    KEY=0; //#################################################################
//#################################################################
    printf(" ============== KEY= %2i   ==\n",KEY);
    printf(" ===========================\n");
    printf(" \n");
    printf(" ===========================================================\n");
    printf(" ============== non-zero contributions to <|ME|^2>_spin   ==\n");
    printf(" ===========================================================\n");
    printf(" \n");

  }
  
  // these loops need to be cleaned from zero contributions! 
  for(int I1=-5;I1<=5;I1++){  // for test of single production process fix flavour
    for(int I2=-5;I2<=5;I2++){  // for test of single production process fix flavour
      for(int I3=-5;I3<=5;I3++){
	for(int I4=-5;I4<=5;I4++){
	  
          int ID1 = I1; if( ID1 == 0 ) ID1 = 21;
          int ID2 = I2; if( ID2 == 0 ) ID2 = 21;
          int ID3 = I3; if( ID3 == 0 ) ID3 = 21;
          int ID4 = I4; if( ID4 == 0 ) ID4 = 21;

          W[0][0] += f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4, 1, -1, P, KEY);    // as in case of nonSM_adopt we change the sign for 
          W[0][1] += f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4, 1,  1, P, KEY);    // second tau helicity assuming without checking that
          W[1][0] += f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4,-1, -1, P, KEY);    // for VBF quantization frame conventions are the same.
          W[1][1] += f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4,-1,  1, P, KEY);

	  if( DEBUGVBF ) {
            if (ID1==51 && ID2==-1 && ID3==4 && ID4==-4 )  /// THIS IS BLOCKED NOW #####################################################
	      { KEY=1;
		double cosik=vbfdistr(ID1,ID2,ID3,ID4, 1, -1, P, KEY)+vbfdistr(ID1,ID2,ID3,ID4, 1,  1, P, KEY)+vbfdistr(ID1,ID2,ID3,ID4,-1, -1, P, KEY)+vbfdistr(ID1,ID2,ID3,ID4,-1,  1, P, KEY);
		std::cout << "                              ID-s = " <<ID1 <<" "<<ID2 <<" "<<ID3 <<" "<<ID4 <<" "<< "          KEY="<<KEY<<std::endl;
		std::cout << "     " <<  std::endl;
		std::cout << " ME^2 summed over spin  = " << cosik << std::endl;
		std::cout << "---------" <<  std::endl;
		std::cout << "   " <<  std::endl;
	      }

	    if( ( W[0][0] > 10) || (W[0][1] > 10) || (W[1][0] > 10) || ( W[1][1] > 10)) { 
	      std::cout << "ERWxxx: ID= " <<ID1 << " " << ID2 << " " << ID3 << " " << ID4 << " W[]= " 
			<< f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4, 1, -1, P, KEY) << " " 
			<< f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4, 1,  1, P, KEY) << " "
			<< f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4,-1, -1, P, KEY) << " "
			<< f_x1[I1]*f_x2[I2]*vbfdistr(ID1,ID2,ID3,ID4,-1,  1, P, KEY)
			<< std::endl; 
	    }
	    if( vbfdistr(ID1,ID2,ID3,ID4, 1, -1, P, KEY)+vbfdistr(ID1,ID2,ID3,ID4, -1, 1, P, KEY)+vbfdistr(ID1,ID2,ID3,ID4, 1, 1, P, KEY) +vbfdistr(ID1,ID2,ID3,ID4, -1, -1, P, KEY)> 0) {
	      float check = vbfdistr(ID1,ID2,ID4,ID3, 1, -1, P, KEY)+vbfdistr(ID1,ID2,ID3,ID4, -1, 1, P, KEY);
	      if( check != 0 ||  check == 0){ 
                int CPSTATUSICP=0;
		printf(" ID-s= %2i %2i %2i %2i  CP used= %1i ## VALUE:  %16.10e   ##  Spin contr.:  (+-)=  %9.3e  (-+)=  %9.3e  (--)=  %9.3e  (++)=  %9.3e  \n", ID1, ID2, ID3,ID4, cpstatus_.icp,
		       vbfdistr(ID1,ID2,ID3,ID4, 1, -1, P, KEY)+ vbfdistr(ID1,ID2,ID3,ID4, -1, 1, P, KEY)+ vbfdistr(ID1,ID2,ID3,ID4, -1, -1, P, KEY)+ vbfdistr(ID1,ID2,ID3,ID4, 1, 1, P, KEY),
		       vbfdistr(ID1,ID2,ID3,ID4,  1, -1, P, KEY),
		       vbfdistr(ID1,ID2,ID3,ID4, -1,  1, P, KEY),
		       vbfdistr(ID1,ID2,ID3,ID4, -1, -1, P, KEY), 
		       vbfdistr(ID1,ID2,ID3,ID4,  1,  1, P, KEY) );
	      }
	    }
	  }

	}
      }
    }
  }


  //  std::cout << "That is it"<< std::endl;
  //exit(-1);

}



/*******************************************************************************
    Calculate weights, case of event record vertex like Z/gamma/H ... -> tau tau decay plus 2 jets.

  Determine taus decay channel, calculates all necessary components from decay HHp HHm and production W[][] for 
  calculation of all weights, later calculates weights.

  Input:        X four momentum of Z/Higgs may be larger than sum of tau1 tau2, missing component
                is assumed to be QED brem four momenta of outgoing partons and taus; vectors of decay products for first and second tau

  Hidden input:  relWTnonSM,  switch of what is  WTnonSM
  Hidden output: WTamplitM or WTamplitP matrix elements of tau1 tau2 decays


                    Polari - helicity attributed to taus, 100% correlations between tau+ and tau-
                    accesible with getTauSpin()
                   WTnonSM weight for introduction of matrix element due to nonSM in cross section, or just ME^2*PDF's for  relWTnonS==false 
  Explicit output: WT spin correlation weight 
*******************************************************************************/
double TauSpinnerWeighter::calculateWeightFromParticlesVBF(SimpleParticle &p3, SimpleParticle &p4,SimpleParticle &sp_X, SimpleParticle &sp_tau1, SimpleParticle &sp_tau2, vector<SimpleParticle> &sp_tau1_daughters, vector<SimpleParticle> &sp_tau2_daughters)
{
  SimpleParticle         sp_tau;
  SimpleParticle         sp_nu_tau;
  vector<SimpleParticle> sp_tau_daughters;
  int KEY=0;
  // here we impose that it is for SM Higgs only
  if(sp_X.pdgid()==25) KEY=1;

  // First iteration is for tau plus, so the 'nu_tau' is tau minus
  if (sp_tau1.pdgid() == -15 )
  {
    sp_tau           = sp_tau1;
    sp_nu_tau        = sp_tau2;
    sp_tau_daughters = sp_tau1_daughters;
  }
  else
  {
    sp_tau           = sp_tau2;
    sp_nu_tau        = sp_tau1;
    sp_tau_daughters = sp_tau2_daughters;
  }

  double *HHp, *HHm;
  
  // We use this to separate namespace for tau+ and tau-
  if(true)
  {
    // Create Particles from SimpleParticles
    Particle X     (      sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
    Particle tau   (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
    Particle nu_tau( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

    vector<Particle> tau_daughters;

    // tau pdgid
    int tau_pdgid = sp_tau.pdgid();

    // Create list of tau daughters
    for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
    {
      Particle pp(sp_tau_daughters[i].px(),
                  sp_tau_daughters[i].py(),
                  sp_tau_daughters[i].pz(),
                  sp_tau_daughters[i].e(),
                  sp_tau_daughters[i].pdgid() );

      tau_daughters.push_back(pp);
    }

    double phi2 = 0.0, theta2 = 0.0;


    //  Move decay kinematics first to tau rest frame  with z axis pointing along nu_tau direction
    //  later rotate again to have neutrino from tau decay along z axis: angles phi2, theta2
    prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);


    //  Determine decay channel and then calculate polarimetric vector HH
    HHp = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);

    DEBUG
    (
      cout<<tau_pdgid<<" -> ";
      for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
      cout<<" (HHp: "<<HHp[0]<<" "<<HHp[1]<<" "<<HHp[2]<<" "<<HHp[3]<<") ";
      cout<<endl;
    )

    WTamplitP = WTamplit;
  } // end of tau+

  // Second iteration is for tau minus, so the 'nu_tau' is tau minus
  if(sp_tau1.pdgid() == 15 )
  {
    sp_tau           = sp_tau1;
    sp_nu_tau        = sp_tau2;
    sp_tau_daughters = sp_tau1_daughters;
  }
  else
  {
    sp_tau           = sp_tau2;
    sp_nu_tau        = sp_tau1;
    sp_tau_daughters = sp_tau2_daughters;
  }
  
  // We use this to separate namespace for tau+ and tau-
  if(true)
  {
    // Create Particles from SimpleParticles
    Particle X     (      sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
    Particle tau   (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
    Particle nu_tau( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

    vector<Particle> tau_daughters;

    // tau pdgid
    int tau_pdgid = sp_tau.pdgid();

    // Create list of tau daughters
    for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
    {
      Particle pp(sp_tau_daughters[i].px(),
                  sp_tau_daughters[i].py(),
                  sp_tau_daughters[i].pz(),
                  sp_tau_daughters[i].e(),
                  sp_tau_daughters[i].pdgid() );

      tau_daughters.push_back(pp);
    }

    double phi2 = 0.0, theta2 = 0.0;


    //  Move decay kinematics first to tau rest frame  with z axis pointing along nu_tau direction
    //  later rotate again to have neutrino from tau decay along z axis: angles phi2, theta2
    prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);


    //  Determine decay channel and then calculate polarimetric vector HH
    HHm = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);

    DEBUG
    (
      cout<<tau_pdgid<<" -> ";
      for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
      cout<<" (HHm: "<<HHm[0]<<" "<<HHm[1]<<" "<<HHm[2]<<" "<<HHm[3]<<") ";
      cout<<endl;
    )

    WTamplitM = WTamplit; 
  } // end of tau-


  double W[2][2] = { { 0.25, 0.25 },
                     { 0.25, 0.25 } };  // this  is trivial W spin (helicity only) density matrix
                                        // consist  of  ME^2*PDF's for production.
                                        // pre-initialization all are equal i.e. no spin effects
  
 
  // calculate ME^2*PDF's for SM
  // we use: sp_nu_tau, sp_tau; funny names - object order for HH calc.
  getME2VBF(p3, p4, sp_X, sp_nu_tau, sp_tau, W, KEY);
  //  if(KEY==0 || KEY==1 )getME2VBF(p3, p4, sp_X, sp_tau1, sp_tau2, W, KEY); 
  //else                 getME2VBF(p3, p4, sp_X, sp_tau1, sp_tau2, W, 0);


  double sum=(W[0][0]+W[0][1]+ W[1][0]+W[1][1]); // getME2VBF calculated PDF's times matrix elements squared for each of four helicity configurations  
                                                 // tau+ and tau- using SM process KEY=0 DY, KEY=1 Higgs. 

  if(nonSM2==0 ) { 
    WTnonSM=1.0;
    if(relWTnonSM==0)  WTnonSM=sum;  // The variable accessible for user stores production weight ME^2 * PDF's summed over flavours and spin states
  }
  if(nonSM2==1) { 
    // now we re-calculate W  using anomalous variant of matrix elements.
    // we use: sp_nu_tau, sp_tau; funny names - object order for HH calc.
    getME2VBF(p3, p4, sp_X, sp_nu_tau, sp_tau, W, KEY+2);

    double sum2=(W[0][0]+W[0][1]+ W[1][0]+W[1][1]);

    WTnonSM=sum2/sum;
    if(relWTnonSM==0)  WTnonSM=sum2;
  }


  double WT = W[0][0]*(1+HHp[2])*(1+HHm[2])+W[0][1]*(1+HHp[2])*(1-HHm[2])+ W[1][0]*(1-HHp[2])*(1+HHm[2])+W[1][1]*(1-HHp[2])*(1-HHm[2]);
  WT = WT/(W[0][0]+W[0][1]+ W[1][0]+W[1][1]);

  // we separate cross section into first tau helicity + and - parts. Second tau follow.
  // This must be tested  especially for  KEY >=2
  // case for scalar (H) has flipped sign of second tau spin? Tests needed
   double RRR = Tauola::randomDouble();  
  Polari=1.0;
  if (RRR<(W[0][0]*(1+HHp[2])*(1+HHm[2])+W[0][1]*(1+HHp[2])*(1-HHm[2]))/(W[0][0]*(1+HHp[2])*(1+HHm[2])+W[0][1]*(1+HHp[2])*(1-HHm[2])+W[1][0]*(1-HHp[2])*(1+HHm[2])+W[1][1]*(1-HHp[2])*(1-HHm[2]))) Polari=-1.0;



  // Print out some info about the channel
  DEBUG( cout<<" WT: "<<WT<<endl; )

  if (WT<0.0) {
    printf("WT is: %13.10f. Setting WT = 0.0\n",WT);
    WT = 0.0;
   }

  if (WT>4.0) {
    printf("WT is: %13.10f. Setting WT = 4.0\n",WT);
    WT = 4.0;
  }

  delete[] HHp;
  delete[] HHm;
  
  return WT; 
}




// ===================================================================
// ATLAS: End of new class based implementation: TauSpinnerWeighter
//        resuming original code
// ===================================================================



























/*****   GLOBAL VARIABLES AND THEIR PRE-INITIALISATION  *****/

double CMSENE = 7000.0;// Center of mass system energy (used by PDF's) 
bool   Ipp    = true;  // pp collisions
int    Ipol   = 1;     // Is the  sample in use polarized? (relevant for Z/gamma case only)
int    nonSM2 = 0;     // Turn on/off nonSM calculations 
int    nonSMN = 0;     // Turn on, if calculations of nonSM weight, is to be used to modify shapes only
int    relWTnonSM = 1; // 1: relWTnonSM is relative to SM; 0: absolute
double WTnonSM=1.0;    // nonSM weight
double Polari =0.0;    // Helicity, attributed to the tau pair. If not attributed then 0.
                       // Polari is attributed for the case when spin effects are taken into account.
                       // Polari is available for the user program with getTauSpin() 
bool IfHiggs=false;    // flag used in  sigborn()
double WTamplit=1;     // AMPLIT weight for the decay last invoked
double WTamplitP=1;    // AMPLIT weight for tau+ decay
double WTamplitM=1;    // AMPLIT weight for tau- decay

// Higgs parameters
int  IfHsimple=0;      // switch for simple Higgs (just Breit-Wigner)
double XMH   = 125.0;  // mass (GeV)
double XGH   = 1.0;    // width, should be detector width, analysis dependent.
double Xnorm = 0.15;   // normalization of Higgs Born cross-section, at hoc

// Transverse components of Higgs spin density matrix
double RXX = 0.0; //-1.0;
double RYY = 0.0; // 1.0;
double RXY = 0.0;
double RYX = 0.0;

// Coefficients for transverse components of Z/gamma spin density matrix
double RzXX = 0.0; //-1.0;
double RzYY = 0.0; // 1.0;
double RzXY = 0.0;
double RzYX = 0.0;

// Values of transverse components of Z/gamma spin density matrix calculated inside getLongitudinalPolarization
double R11 = 0.0;
double R22 = 0.0;
double R12 = 0.0;  // for future use 
double R21 = 0.0;  // for future use
 
/***** END: GLOBAL VARIABLES AND THEIR PRE-INITIALISATION  *****/

double f(double x, int ID, double SS, double cmsene)
// PDF's parton density function divided by x;
// x - fraction of parton momenta
// ID flavour of incoming quark
// SS scale of hard process
// cmsene center of mass for pp collision.
{
  // LHAPDF manual: http://projects.hepforge.org/lhapdf/manual
  //  double xfx(const double &x;, const double &Q;, int fl);
  // returns xf(x, Q) for flavour fl - this time the flavour encoding
  // is as in the LHAPDF manual...
  // -6..-1 = tbar,...,ubar, dbar
  // 1..6 = duscbt
  // 0 = g
  // xfx is the C++ wrapper for fortran evolvePDF(x,Q,f)

  // note that SS=Q^2, make the proper choice of PDFs arguments.
  return LHAPDF::xfx(x, sqrt(SS), ID)/x;

  //return x*(1-x);

}

  // Calculates Born cross-section summed over final taus spins.
  // Input parameters: 
  // incoming flavour                    ID  
  // invariant mass^2                    SS  
  // scattering angle                    costhe
  // Hidden input (type of the process): IfHiggs, IfHsimple
double sigborn(int ID, double SS, double costhe)
{
  //  cout << "ID : " << ID << " HgsWTnonSM = " << HgsWTnonSM << " IfHsimple = " << IfHsimple << endl;
  // BORN x-section.
  // WARNING: overall sign of costheta must be fixed
  int tauID = 15;

  // case of the Higgs boson
    if (IfHiggs) {
      double SIGggHiggs=0.;
      // for the time being only for  gluon it is non-zero. 
      if(ID==0){        
        int IPOne =  1;
        int IMOne = -1;
        SIGggHiggs=disth_(&SS, &costhe, &IPOne, &IPOne)+disth_(&SS, &costhe, &IPOne, &IMOne)+
  	           disth_(&SS, &costhe, &IMOne, &IPOne)+disth_(&SS, &costhe, &IMOne, &IMOne);


        double PI=3.14159265358979324;
	SIGggHiggs *=  XMH * XMH * XMH *XGH /PI/ ((SS-XMH*XMH)*(SS-XMH*XMH) + XGH*XGH*XMH*XMH);   
	//	cout << "JK: SIGggHiggs = " << SS << " " << XMH << " " << XGH << " " <<  XMH * XMH * XMH * XGH /PI/ ((SS-XMH*XMH)*(SS-XMH*XMH) + XGH*XGH*XMH*XMH) << " " << SIGggHiggs << endl;

        if(IfHsimple==1) SIGggHiggs =  Xnorm * XMH * XGH /PI/ ((SS-XMH*XMH)*(SS-XMH*XMH) +XGH*XGH*XMH*XMH);
	//	cout << "ZW: SIGggHiggs = " << SS << " " << costhe << " " << SIGggHiggs << endl;
      }
    return SIGggHiggs;
    }

  // case of Drell-Yan

  if (ID==0) return 0.0 ;   // for the time being for gluon it is zero.
  if (ID>0) initwk_( &ID, &tauID, &SS);
  else
  {
    ID = -ID;
    initwk_( &ID, &tauID, &SS);
  }

  int    iZero = 0;
  double dOne  =  1.0;
  double dMOne = -1.0;
  // sum DY Born over all tau helicity configurations:
  return ( t_born_(&iZero, &SS, &costhe, &dOne , &dOne) + t_born_(&iZero, &SS, &costhe, &dOne , &dMOne)
	   + t_born_(&iZero, &SS, &costhe, &dMOne, &dOne) + t_born_(&iZero, &SS, &costhe, &dMOne, &dMOne))/SS/123231.; 
// overall norm. factor .../SS/123231  most probably it is alpha_QED**2/pi/2/SS is from comparison between Born we use and Born used in Warsaw group. 
}

/*******************************************************************************
  Initialize TauSpinner

  Print info and set global variables
*******************************************************************************/
void initialize_spinner(bool _Ipp, int _Ipol, int _nonSM2, int _nonSMN, double _CMSENE)
{
  Ipp    = _Ipp;
  Ipol   = _Ipol;
  nonSM2 = _nonSM2;
  nonSMN = _nonSMN;

  CMSENE = _CMSENE;

  cout<<" ------------------------------------------------------"<<endl;
  cout<<" TauSpinner v2.0.1"<<endl;
  cout<<" -----------------"<<endl;
  cout<<"   19.May.2016    "<<endl;
  cout<<" by Z. Czyczula (until 2015), T. Przedzinski, E. Richter-Was, Z. Was,"<<endl;
  cout<<"  matrix elements implementations "<<endl;
  cout<<"  also J. Kalinowski and W. Kotlarski"<<endl;
  cout<<" ------------------------------------------------------"<<endl;
  cout<<" Ipp - true for pp collision; otherwise polarization"<<endl;
  cout<<"       of individual taus from Z/gamma* is set to 0.0"<<endl;
  cout<<" Ipp    = "<<Ipp<<endl;
  cout<<" CMSENE - used in PDF calculations; only if Ipp = true"<<endl;
  cout<<"          and only for Z/gamma*"<<endl;
  cout<<" CMSENE = "<<CMSENE<<endl;
  cout<<" Ipol - relevant for Z/gamma* decays "<<endl;
  cout<<" 0 - events generated without spin effects                "<<endl;
  cout<<" 1 - events generated with all spin effects               "<<endl;
  cout<<" 2 - events generated with spin correlations and <pol>=0  "<<endl;
  cout<<" 3 - events generated with spin correlations and"<<endl;
  cout<<"     polarization but missing angular dependence of <pol>"<<endl;
  cout<<" Ipol    = "<<Ipol<<endl;
  cout<<" Ipol - relevant for Z/gamma* decays "<<endl;
  cout<<" NOTE: For Ipol=0,1 algorithm is identical.               "<<endl;
  cout<<"       However in user program role of wt need change.    "<<endl;
  cout<<" nonSM2  = "<<nonSM2<<endl;
  cout<<" 1/0 extra term in cross section, density matrix on/off   "<<endl;
  cout<<" nonSMN  = "<<nonSMN<<endl;
  cout<<" 1/0 extra term in cross section, for shapes only? on/off "<<endl;
  cout<<" note KEY - for options of matrix elements calculations   "<<endl;
  cout<<"            in cases of final states  with two jets       "<<endl;
  cout<<" ------------------------------------------------------   "<<endl;
}

/*******************************************************************************
  Set flag for calculating relative(NONSM-SM)/absolute weight for X-section
  calculated as by product in longitudinal polarization method.
  1: relWTnonSM is relative to SM (default)
  0: absolute
*******************************************************************************/
void setRelWTnonSM(int _relWTnonSM)
{
  relWTnonSM = _relWTnonSM;
}

/*******************************************************************************
  Set Higgs mass, width and normalization of Higgs born function
  Default is mass = 125, width = 1.0, normalization = 0.15
*******************************************************************************/
  void setHiggsParameters(int jak, double mass, double width, double normalization)
{
  IfHsimple=jak;
  XMH   = mass;
  XGH   = width;
  Xnorm = normalization;
}

/*******************************************************************************
  Set transverse components of Higgs spin density matrix
*******************************************************************************/
void setHiggsParametersTR(double Rxx, double Ryy, double Rxy, double Ryx)
{
  
  RXX = Rxx;
  RYY = Ryy;
  RXY = Rxy;
  RYX = Ryx;
}


/*******************************************************************************
  Set coefficients for transverse components of Z/gamma spin density matrix
*******************************************************************************/
void setZgamMultipliersTR(double Rxx, double Ryy, double Rxy, double Ryx)
{
  RzXX = Rxx;
  RzYY = Ryy;
  RzXY = Rxy;
  RzYX = Ryx;
}

/*******************************************************************************
  Get  transverse components of Z/gamma spin density matrix
*******************************************************************************/
void getZgamParametersTR(double &Rxx, double &Ryy, double &Rxy, double &Ryz)
{
  Rxx = R11;
  Ryy = R22;
  Rxy = 0.0;
  Ryz = 0.0;
}

/*******************************************************************************
  Get Higgs mass, width and normalization of Higgs born function
*******************************************************************************/
void getHiggsParameters(double *mass, double *width, double *normalization)
{
  *mass          = XMH;
  *width         = XGH;
  *normalization = Xnorm;
}

/*******************************************************************************
  Set type of spin treatment used in the sample
  Ipol = 0   sample was not polarised
  Ipol = 1   sample was having complete longitudinal spin effects 
  Ipol = 2   sample was featuring longitudinal spin correlations only,
             but not dependence on polarisation due to couplings of the Z
  Ipol = 3   as in previous case, but only angular dependence of spin polarisation
             was missing in the sample
*******************************************************************************/
void setSpinOfSample(int _Ipol)
{
  Ipol = _Ipol;
}

/*******************************************************************************
  Turn nonSM calculation of Born cross-section on/off
*******************************************************************************/
void setNonSMkey(int _key)
{
  nonSM2 = _key;
}

/*******************************************************************************
  Get nonSM weight
*******************************************************************************/
double getWtNonSM()
{
  return WTnonSM;
}
/*******************************************************************************
  Get weights for tau+ tau- decay matrix elements
*******************************************************************************/
double getWtamplitP(){return WTamplitP;}
double getWtamplitM(){return WTamplitM;}

/*******************************************************************************
  Get tau spin (helicities of tau+ tau- are 100% correlated)
  Use after sample is reweighted to obtain information on attributed  tau 
  longitudinal spin projection.
*******************************************************************************/
double getTauSpin()
{
  return Polari;
}

/*******************************************************************************
  Calculate weights, case of event record vertex like W -> tau nu_tau decay.
  Function for W+/- and H+/-

  Determines decay channel, calculates all necessary components for 
  calculation of all weights, calculates weights.
  Input:        X four momentum may be larger than sum of tau nu_tau, missing component
                is assumed to be QED brem
  Hidden input: none
  Hidden output: WTamplitM or WTamplitP of tau decay (depending on tau charge)
                 NOTE: weight for sp_X production matrix elements is not calculated 
                 for decays of charged intermediate W+-/H+-! 
  Explicit output: WT spin correlation weight 
*******************************************************************************/
double calculateWeightFromParticlesWorHpn(SimpleParticle &sp_X, SimpleParticle &sp_tau, SimpleParticle &sp_nu_tau, vector<SimpleParticle> &sp_tau_daughters)
{
  // Create Particles from SimpleParticles

  Particle X     (     sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
  Particle tau   (   sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
  Particle nu_tau(sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

  vector<Particle> tau_daughters;

  // tau pdgid
  int tau_pdgid = sp_tau.pdgid();

  // Create vector of tau daughters
  for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
  {
    Particle pp(sp_tau_daughters[i].px(),
                sp_tau_daughters[i].py(),
                sp_tau_daughters[i].pz(),
                sp_tau_daughters[i].e(),
                sp_tau_daughters[i].pdgid() );

    tau_daughters.push_back(pp);
  }

  double phi2 = 0.0, theta2 = 0.0;

  //  To calcluate matrix elements TAUOLA need tau decay products in tau rest-frame: we first boost all products
  //  to tau rest frame (tau nu_tau of W decay along z-axis, intermediate step for boost is tau nu_tau (of W decay) rest-frame), 
  //  then rotate to have neutrino from tau decay along z axis; 
  //  calculated for that purpose angles phi2, theta2 are stored for rotation back of HH
  prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);


  //  Identify decay channel and then calculate polarimetric vector HH; calculates also WTamplit
  double *HH = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);
 
  double sign = 1.0;  // tau from W is 100 % polarized, also from charged Higgs (but with opposite sign)
  if     ( abs(sp_X.pdgid()) == 24 ) { sign= 1.0; } 
  else if( abs(sp_X.pdgid()) == 37 ) { sign=-1.0; } 
  else
  {
    cout<<"wrong sp_W/H.pdgid()="<<sp_X.pdgid()<<endl;
    exit(-1);
  }
  if     (sp_X.pdgid() > 0 )   
    {WTamplitM = WTamplit;}   // tau- decay matrix element^2, spin averaged.
  else
    {WTamplitP = WTamplit;}   // tau+ decay matrix element^2, spin averaged.

  // spin correlation weight. Tau production matrix element is represented by `sign'
  double WT   = 1.0+sign*HH[2];     // [2] means 'pz' component

  // Print out some info about the channel
  DEBUG
  (
    cout<<tau_pdgid<<" -> ";
    for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
    cout<<" (HH: "<<HH[0]<<" "<<HH[1]<<" "<<HH[2]<<" "<<HH[3]<<") WT: "<<WT<<endl;
  )

  // TP:31Nov2013 checks of possible problems: weight outside theoretically allowed range
  if (WT<0.0) {
    printf("TauSpinner::calculateWeightFromParticlesWorHpn WT is: %13.10f. Setting WT = 0.0\n",WT);
    WT = 0.0;
   }

  if (WT>2.0) {
    printf("Tauspinner::calculateWeightFromParticlesWorHpn WT is: %13.10f. Setting WT = 2.0\n",WT);
    WT = 2.0;
  }

  delete HH;
  
  return WT;
}

/*******************************************************************************
  Calculate weights, case of event record vertex like Z/gamma/H ... -> tau tau decay.

  Determine decay channel, calculates all necessary components for 
  calculation of all weights, calculates weights.

  Input:        X four momentum may be larger than sum of tau1 tau2, missing component
                is assumed to be QED brem

  Hidden input:  relWTnonS, nonSM2 (used only in  getLongitudinalPolarization(...) )
  Hidden output: WTamplitM or WTamplitP of tau1 tau2 decays
                 weight for sp_X production matrix element^2 is calculated inside 
                 plzap2
                 Polari - helicity attributed to taus, 100% correlations between tau+ and tau-
                 WTnonSM  
  Explicit output: WT spin correlation weight 
*******************************************************************************/
double calculateWeightFromParticlesH(SimpleParticle &sp_X, SimpleParticle &sp_tau1, SimpleParticle &sp_tau2, vector<SimpleParticle> &sp_tau1_daughters, vector<SimpleParticle> &sp_tau2_daughters)
{
  //  cout << "sp_tau1_daughters = " << sp_tau1_daughters.size() << endl;
  //  cout << "sp_tau2_daughters = " << sp_tau2_daughters.size() << endl;
  SimpleParticle         sp_tau;
  SimpleParticle         sp_nu_tau;
  vector<SimpleParticle> sp_tau_daughters;


  // First we calculate HH for tau+  
  // We enforce that sp_tau is tau+ so the 'nu_tau' is tau-
  if (sp_tau1.pdgid() == -15 )
  {
    sp_tau           = sp_tau1;
    sp_nu_tau        = sp_tau2;
    sp_tau_daughters = sp_tau1_daughters;
  }
  else
  {
    sp_tau           = sp_tau2;
    sp_nu_tau        = sp_tau1;
    sp_tau_daughters = sp_tau2_daughters;
  }

  double *HHp, *HHm;
  
  // We use artificial if(true){... } construction to separate namespace for tau+ and tau-
  if(true) 
  {
    // Create Particles from SimpleParticles
    Particle X     (      sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
    Particle tau   (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
    Particle nu_tau( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

    vector<Particle> tau_daughters;

    // tau pdgid
    int tau_pdgid = sp_tau.pdgid();

    // Create list of tau daughters
    for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
    {
      Particle pp(sp_tau_daughters[i].px(),
                  sp_tau_daughters[i].py(),
                  sp_tau_daughters[i].pz(),
                  sp_tau_daughters[i].e(),
                  sp_tau_daughters[i].pdgid() );

      tau_daughters.push_back(pp);
    }

    double phi2 = 0.0, theta2 = 0.0;

    //  To calculate matrix elements TAUOLA need tau decay products in tau rest-frame: we first boost all products
    //  to tau rest frame (tau other tau  of Z/H decay along z-axis, intermediate step for boost is tau-tau pair rest-frame), 
    //  then rotate to have neutrino from tau decay along z axis; 
    //  calculated for that purpose angles phi2, theta2 are stored for rotation back of HHp
    prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);
 



    //  Identify decay channel and then calculate polarimetric vector HH; calculates also WTamplit
    HHp = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);

    DEBUG
    (
      cout<<tau_pdgid<<" -> ";
      for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
      cout<<" (HHp: "<<HHp[0]<<" "<<HHp[1]<<" "<<HHp[2]<<" "<<HHp[3]<<") ";
      cout<<endl;
    )

    WTamplitP = WTamplit;
  } // end of if(true);  for tau+



  // Second we calculate HH for tau-  
  // We enforce that sp_tau is tau- so the 'nu_tau' is tau+
  if(sp_tau1.pdgid() == 15 )
  {
    sp_tau           = sp_tau1;
    sp_nu_tau        = sp_tau2;
    sp_tau_daughters = sp_tau1_daughters;
  }
  else
  {
    sp_tau           = sp_tau2;
    sp_nu_tau        = sp_tau1;
    sp_tau_daughters = sp_tau2_daughters;
  }
  
  // We use artificial if(true){... } construction to separate namespace for tau+ and tau-
  if(true)
  {
    // Create Particles from SimpleParticles
    Particle X     (      sp_X.px(),      sp_X.py(),      sp_X.pz(),      sp_X.e(),      sp_X.pdgid() );
    Particle tau   (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
    Particle nu_tau( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

    vector<Particle> tau_daughters;

    // tau pdgid
    int tau_pdgid = sp_tau.pdgid();

    // Create list of tau daughters
    for(unsigned int i=0; i<sp_tau_daughters.size(); i++)
    {
      Particle pp(sp_tau_daughters[i].px(),
                  sp_tau_daughters[i].py(),
                  sp_tau_daughters[i].pz(),
                  sp_tau_daughters[i].e(),
                  sp_tau_daughters[i].pdgid() );

      tau_daughters.push_back(pp);
    }

    double phi2 = 0.0, theta2 = 0.0;


    //  To calculate matrix elements TAUOLA need tau decay products in tau rest-frame: we first boost all products
    //  to tau rest frame (tau other tau  of Z/H decay along z-axis, intermediate step for boost is tau-tau pair rest-frame), 
    //  then rotate to have neutrino from tau decay along z axis; 
    //  calculated for that purpose angles phi2, theta2 are stored for rotation back of HHm
    prepareKinematicForHH   (tau, nu_tau, tau_daughters, &phi2, &theta2);


    //  Identify decay channel and then calculate polarimetric vector HHm; calculates also WTamplit
    HHm = calculateHH(tau_pdgid, tau_daughters, phi2, theta2);

    DEBUG
    (
      cout<<tau_pdgid<<" -> ";
      for(unsigned int i=0;i<tau_daughters.size();i++) cout<<tau_daughters[i].pdgid()<<" ";
      cout<<" (HHm: "<<HHm[0]<<" "<<HHm[1]<<" "<<HHm[2]<<" "<<HHm[3]<<") ";
      cout<<endl;
    )

    WTamplitM = WTamplit; 
  } // end of if(true);  for tau-

  // CALCULATION OF PRODUCTION MATRIX ELEMENTS, THEN SPIN WEIGHTS AND FURTHER HIDDEN OUTPUTS

  // sign, in ultrarelativistic limit it is  component of spin density matrix: 
  // longitudinal spin correlation for intermediate vector state gamma*, 
  // for Z etc. sign= +1; for Higgs, other scalars, pseudoscalars sign= -1
  double sign = 1.0; 
  if(sp_X.pdgid() == 25) { sign=-1.0; } 
  if(sp_X.pdgid() == 36) { sign=-1.0; }
  if(sp_X.pdgid() ==553) { sign=-1.0; }  // upsilon(1s) can be treated as scalar

  double WT = 0.0;

  Polari = 0.0;  
  
  if(sign == -1.0) // Case of scalar
  {
    double S = sp_X.e()*sp_X.e() - sp_X.px()*sp_X.px() - sp_X.py()*sp_X.py() - sp_X.pz()*sp_X.pz();
    IfHiggs=true; //global variable
    double pol = getLongitudinalPolarization(S, sp_tau, sp_nu_tau);  // makes sense only for nonSM otherwise NaN

    if(nonSM2==1) // WARNING it is for spin=2 resonance!!
      {
     
      double corrX2;
      double polX2;

      // NOTE: in this case, sp_nu_tau is the 2nd tau
      //      nonSMHcorrPol(S, sp_tau, sp_nu_tau, &corrX2, &polX2); // for future use
      //                                                          WARNING: may be for polX2*HHm[2] we need to fix sign!
      polX2=pol;
      corrX2=-sign; // if X2 is of spin=2, spin correlation like for Z, we use RzXX,RzYY,RzXY,RzYX as transverse components of density matrix 
      
      WT = 1.0+corrX2*HHp[2]*HHm[2]+polX2*HHp[2]+polX2*HHm[2] + RzXX*HHp[0]*HHm[0] + RzYY*HHp[1]*HHm[1] + RzXY*HHp[0]*HHm[1] + RzYX*HHp[1]*HHm[0];

      // we separate cross section into helicity parts. From this, we attribute helicity states to taus: ++ or -- 
      double RRR = Tauola::randomDouble();  
      Polari=1.0;
      if (RRR<(1.0+polX2)*(1.0+corrX2*HHp[2]*HHm[2]+HHp[2]+HHm[2])/(2.0+2.0*corrX2*HHp[2]*HHm[2]+2.0*polX2*HHp[2]+2.0*polX2*HHm[2])) Polari=-1.0;    
      }
    else  // case of Higgs
      {
      WT = 1.0 + sign*HHp[2]*HHm[2] + RXX*HHp[0]*HHm[0] + RYY*HHp[1]*HHm[1] + RXY*HHp[0]*HHm[1] + RYX*HHp[1]*HHm[0];

      //  we separate cross section into helicity parts. From this, we attribute helicity states to taus: +- or -+ 
      double RRR = Tauola::randomDouble();  
      Polari=1.0;
      if (RRR<(1.0+sign*HHp[2]*HHm[2]+HHp[2]-HHm[2])/(2.0+2.0*sign*HHp[2]*HHm[2])) Polari=-1.0;
      }
  }
  else   // Case of Drell Yan
  { 

    double S = sp_X.e()*sp_X.e() - sp_X.px()*sp_X.px() - sp_X.py()*sp_X.py() - sp_X.pz()*sp_X.pz();

    // Get Z polarization
    // ( Variable names are misleading! sp_tau is tau+ and sp_nu_tau is tau- )

    IfHiggs=false;
    double pol = getLongitudinalPolarization(S, sp_tau, sp_nu_tau);


    WT = 1.0+sign*HHp[2]*HHm[2]+pol*HHp[2]+pol*HHm[2] + RzXX*R11*HHp[0]*HHm[0] - RzYY*R22*HHp[1]*HHm[1] + RzXY*R12*HHp[0]*HHm[1] + RzYX*R21*HHp[1]*HHm[0]; 
    // in future we may need extra factor for wt which is
    //     F=PLWEIGHT(IDE,IDF,SVAR,COSTHE,1)
    // but it is then non standard version of the code.

    // to correct when in the sample  only spin corr. are in, but no polarization
    if(Ipol==2) WT = WT/(1.0+sign*HHp[2]*HHm[2]); 

    // to correct sample when  corr. are in,  pol. is in, but angular
    // dependence of pol is missing.
    if(Ipol==3)
    {
      // valid for configurations close to Z peak, otherwise approximation is at use.
      double polp1 = plzap2(11,sp_tau.pdgid(),S,0.0);
      double pol1 =(2*(1-polp1)-1) ;
      WT = WT/(1.0+sign*HHp[2]*HHm[2]+pol1*HHp[2]+pol1*HHm[2]); 
    } 

    //  we separate cross section into helicity parts. From this, we attribute helicity states to taus: ++ or -- 
    double RRR = Tauola::randomDouble();  
    Polari=1.0;
    if (RRR<(1.0+pol)*(1.0+sign*HHp[2]*HHm[2]+HHp[2]+HHm[2])/(2.0+2.0*sign*HHp[2]*HHm[2]+2.0*pol*HHp[2]+2.0*pol*HHm[2])) Polari=-1.0; 
  }

  // Print out some info about the channel
  DEBUG( cout<<" WT: "<<WT<<endl; )

  if (WT<0.0) {
    printf("Tauspinner::calculateWeightFromParticlesH WT is: %13.10f. Setting WT = 0.0\n",WT);
    WT = 0.0; // SwB:23Feb2013
   }

  if (WT>4.0) {
    printf("Tauspinner::calculateWeightFromParticlesH WT is: %13.10f. Setting WT = 4.0\n",WT);
    WT = 4.0; // SwB:23Feb2013
  }

  if( WT>4.0 || WT<0.0)
  {
    cout<<"Tauspinner::calculateWeightFromParticlesH ERROR: Z/gamma* or H, and WT not in range [0,4]."<<endl;
    exit(-1);
  }

  if (sign==-1.0 && nonSM2!=1) {
    if (WT>2.0) {
      WT = 2.0; // SwB:26Feb2013
      cout << "Tauspinner::calculateWeightFromParticlesH Setting WT to be 2.0" << endl;
    }
  }

  if( sign==-1.0 && (WT>2.0 || WT<0.0) && nonSM2!=1)
  {
    cout<<"Tauspinner::calculateWeightFromParticlesH ERROR: H and WT not in range [0,2]."<<endl;
    exit(-1);
  }

  delete[] HHp;
  delete[] HHm;
  
  return WT;
}

/*******************************************************************************
  Prepare kinematics for HH calculation
  
  Boost particles to effective bozon rest frame, and rotate them so that tau is on Z axis.
  Then rotate again with theta2 phi2 so neutrino from tau decay is along Z.
*******************************************************************************/
void prepareKinematicForHH(Particle &tau, Particle &nu_tau, vector<Particle> &tau_daughters, double *phi2, double *theta2)
{
  Particle P_QQ( tau.px()+nu_tau.px(), tau.py()+nu_tau.py(), tau.pz()+nu_tau.pz(), tau.e()+nu_tau.e(), 0 );

  //cout<<endl<<"START: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);
  
  // 1) boost tau, nu_tau and tau daughters to rest frame of P_QQ

  tau.boostToRestFrame(P_QQ);
  nu_tau.boostToRestFrame(P_QQ);

  for(unsigned int i=0; i<tau_daughters.size();i++)
    tau_daughters[i].boostToRestFrame(P_QQ);

  //cout<<endl<<"AFTER 1: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);

  // 2) Rotate tau, nu_tau~, tau daughters to frame where tau is along Z
  //    We set accompanying neutino in direction of Z+

  double phi = tau.getAnglePhi();

  tau.rotateXY(-phi);

  double theta   = tau.getAngleTheta();

  tau.rotateXZ(M_PI-theta);

  nu_tau.rotateXY(-phi  );
  nu_tau.rotateXZ(M_PI-theta);

  for(unsigned int i=0; i<tau_daughters.size();i++)
  {
    tau_daughters[i].rotateXY(-phi  );
    tau_daughters[i].rotateXZ(M_PI-theta);
  }

  //cout<<endl<<"AFTER 2: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);

  // 3) boost tau_daughters along Z to rest frame of tau

  for(unsigned int i=0; i<tau_daughters.size();i++)
    tau_daughters[i].boostAlongZ(-tau.pz(),tau.e());

  //cout<<endl<<"AFTER 3: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);

  // 4) Now rotate tau daughters second time
  //    so that nu_tau (from tau daughters list) is on Z axis

  //    We can not be sure  if tau_daughters[0] is neutrino !!!
  //    That is the case, for tauola generated samples, but not in general!
  unsigned int i_stored = 0;

  *phi2=0;
  *theta2=0;

  for(unsigned int i=0; i<tau_daughters.size();i++)
  {
    if(abs(tau_daughters[i].pdgid())==16){
     *phi2     = tau_daughters[i].getAnglePhi();

     tau_daughters[i].rotateXY( -(*phi2)   );

     *theta2   = tau_daughters[i].getAngleTheta();

     tau_daughters[i].rotateXZ( -(*theta2) );
     
     i_stored = i;
     break;
    }
  }

  for(unsigned int i=0; i<tau_daughters.size();i++)
  {
    if(i != i_stored) {
      tau_daughters[i].rotateXY( -(*phi2)   );
      tau_daughters[i].rotateXZ( -(*theta2) );
    }
  }

  //cout<<endl<<"AFTER 4: "<<endl;
  //print(P_QQ,nu_tau,tau,tau_daughters);
}

/*******************************************************************************
  Calculates polarimetric vector HH of the tau decay. 

  HH[3] is timelike because we use FORTRAN methods to calculate HH.
  First decide what is the channel. After that, 4-vectors
  are moved to tau rest frame of tau.
  Polarimetric vector HH is then rotated using angles phi and theta.

  Order of the tau decay products does not matter (it is adjusted) 
*******************************************************************************/
// ATLAS NOTE: this function has been replaced by a newer version from 
// TauSpinner developers, which accounts for decays with kaons
double* calculateHH(int tau_pdgid, vector<Particle> &tau_daughters, double phi, double theta)
{
  int    channel = 0;
  double *HH     = new double[4];

  HH[0]=HH[1]=HH[2]=HH[3]=0.0;  

  vector<int>  pdgid;

  // Create list of tau daughters pdgid
  for(unsigned int i=0; i<tau_daughters.size(); i++)
    pdgid.push_back( tau_daughters[i].pdgid() );

  // 17.04.2014: If Tauola++ is used for generation, 
  // jaki_.ktom  may be changed to 11 at the time of storing decay to event record 
  // (case of full spin effects).
  // For Tauola++ jaki_.ktom is later of no use so 11 does not create problems.
  // For TauSpinner processing, jaki_.ktom should always be 1
  // This was the problem if Tauola++ generation and TauSpinner were used simultaneously.
  jaki_.ktom = 1;

  // tau^- --> pi^- nu_tau
  // tau^+ --> pi^+ anti_nu_tau
  // tau^- --> K^-  nu_tau
  // tau^+ --> K^+  anti_nu_tau
  if( pdgid.size()==2 &&
      (
        ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211) ) ||
        ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211) ) ||
        ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321) ) ||
        ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321) )
      )
    ) {
    channel = 3; // channel: numbering convention of TAUOLA
    if(abs(pdgid[1])==321) channel = 6;
    DEBUG( cout<<"Channel "<<channel<<"  : "; )
    //        PXQ=AMTAU*EPI
    //        PXN=AMTAU*ENU
    //        QXN=PPI(4)*PNU(4)-PPI(1)*PNU(1)-PPI(2)*PNU(2)-PPI(3)*PNU(3)

    //        BRAK=(GV**2+GA**2)*(2*PXQ*QXN-AMPI**2*PXN)
    //        HV(I)=-ISGN*2*GA*GV*AMTAU*(2*PPI(I)*QXN-PNU(I)*AMPI**2)/BRAK

    const double AMTAU = 1.777;
    // is mass of the Pi+- OR K+-
    double AMPI  = sqrt(tau_daughters[1].e() *tau_daughters[1].e()
                       -tau_daughters[1].px()*tau_daughters[1].px()
                       -tau_daughters[1].py()*tau_daughters[1].py()
                       -tau_daughters[1].pz()*tau_daughters[1].pz());

    // two-body decay is so simple, that matrix element is calculated here
    double PXQ=AMTAU*tau_daughters[1].e();
    double PXN=AMTAU*tau_daughters[0].e();
    double QXN=tau_daughters[1].e()*tau_daughters[0].e()-tau_daughters[1].px()*tau_daughters[0].px()-tau_daughters[1].py()*tau_daughters[0].py()-tau_daughters[1].pz()*tau_daughters[0].pz();
    double BRAK=(2*PXQ*QXN-AMPI*AMPI*PXN);

    WTamplit = (1.16637E-5)*(1.16637E-5)*BRAK/2.;//AMPLIT=(GFERMI)**2*BRAK/2. //WARNING: Note for normalisation Cabbibo angle is missing!
    HH[0] = AMTAU*(2*tau_daughters[1].px()*QXN-tau_daughters[0].px()*AMPI*AMPI)/BRAK;
    HH[1] = AMTAU*(2*tau_daughters[1].py()*QXN-tau_daughters[0].py()*AMPI*AMPI)/BRAK;
    HH[2] = AMTAU*(2*tau_daughters[1].pz()*QXN-tau_daughters[0].pz()*AMPI*AMPI)/BRAK;
    HH[3] = 1.0;
  }

  // tau^- --> pi^- pi^0 nu_tau
  // tau^+ --> pi^+ pi^0 anti_nu_tau
  // tau^- --> K^- K^0 nu_tau;          K^0 may be K_L K_S too.
  // tau^+ --> K^+ K^0 anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 111) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 111) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 311) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 311) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 310) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 310) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 130) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 130) )
 
           )
         ) {

    channel = 4;
    DEBUG( cout<<"Channel "<<channel<<"  : "; )
    //      PRODPQ=PT(4)*QQ(4)
    //      PRODNQ=PN(4)*QQ(4)-PN(1)*QQ(1)-PN(2)*QQ(2)-PN(3)*QQ(3)
    //      PRODPN=PT(4)*PN(4)
    //      BRAK=(GV**2+GA**2)*(2*PRODPQ*PRODNQ-PRODPN*QQ2)
    //      HV(I)=2*GV*GA*AMTAU*(2*PRODNQ*QQ(I)-QQ2*PN(I))/BRAK

    const double AMTAU = 1.777;

    int   MNUM = 0;
    if(tau_daughters[2].pdgid() != 111) { MNUM=3; channel = 22;} // sub case of decay to K-K0
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float AMPLIT = 0.0;
    float HV[4]  = { 0.0 };

    dam2pi_( &MNUM, PT, PN, PIM1, PIM2, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> K^-  pi^0 nu_tau
  // tau^+ --> K^+  pi^0 anti_nu_tau
  // tau^- --> pi^- K_S0 nu_tau
  // tau^+ --> pi^+ K_S0 anti_nu_tau
  // tau^- --> pi^- K_L0 nu_tau
  // tau^+ --> pi^+ K_L0 anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 130) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 130) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 310) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 310) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 311) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 311) ) ||
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321, 111) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 111) )
           )
         ) {

    channel = 7;
    DEBUG( cout<<"Channel "<<channel<<"  : "; )
    //      PRODPQ=PT(4)*QQ(4)
    //      PRODNQ=PN(4)*QQ(4)-PN(1)*QQ(1)-PN(2)*QQ(2)-PN(3)*QQ(3)
    //      PRODPN=PT(4)*PN(4)
    //      BRAK=(GV**2+GA**2)*(2*PRODPQ*PRODNQ-PRODPN*QQ2)
    //      HV(I)=2*GV*GA*AMTAU*(2*PRODNQ*QQ(I)-QQ2*PN(I))/BRAK

    const double AMTAU = 1.777;

    double QQ[4];
    QQ[0]=tau_daughters[1].e() -tau_daughters[2].e() ;
    QQ[1]=tau_daughters[1].px()-tau_daughters[2].px();
    QQ[2]=tau_daughters[1].py()-tau_daughters[2].py();
    QQ[3]=tau_daughters[1].pz()-tau_daughters[2].pz();

    double PKS[4];
    PKS[0]=tau_daughters[1].e() +tau_daughters[2].e() ;
    PKS[1]=tau_daughters[1].px()+tau_daughters[2].px();
    PKS[2]=tau_daughters[1].py()+tau_daughters[2].py();
    PKS[3]=tau_daughters[1].pz()+tau_daughters[2].pz();

    // orthogonalization of QQ wr. to PKS
    double PKSD=PKS[0]*PKS[0]-PKS[1]*PKS[1]-PKS[2]*PKS[2]-PKS[3]*PKS[3];
    double QQPKS=QQ[0]*PKS[0]-QQ[1]*PKS[1]-QQ[2]*PKS[2]-QQ[3]*PKS[3];

    QQ[0]=QQ[0]-PKS[0]*QQPKS/PKSD;
    QQ[1]=QQ[1]-PKS[1]*QQPKS/PKSD;
    QQ[2]=QQ[2]-PKS[2]*QQPKS/PKSD;
    QQ[3]=QQ[3]-PKS[3]*QQPKS/PKSD;

    double PRODPQ=AMTAU*QQ[0];
    double PRODNQ=tau_daughters[0].e() *QQ[0]
                 -tau_daughters[0].px()*QQ[1]
                 -tau_daughters[0].py()*QQ[2]
                 -tau_daughters[0].pz()*QQ[3];
    double PRODPN=AMTAU*tau_daughters[0].e();
    double QQ2   =QQ[0]*QQ[0]-QQ[1]*QQ[1]-QQ[2]*QQ[2]-QQ[3]*QQ[3];

    // in this case matrix element is calculated here
    double BRAK=(2*PRODPQ*PRODNQ-PRODPN*QQ2);

    WTamplit = (1.16637E-5)*(1.16637E-5)*BRAK/2.;//AMPLIT=(GFERMI)**2*BRAK/2. WARNING: Note for normalisation Cabibbo angle is missing!
    HH[0]=AMTAU*(2*PRODNQ*QQ[1]-QQ2*tau_daughters[0].px())/BRAK;
    HH[1]=AMTAU*(2*PRODNQ*QQ[2]-QQ2*tau_daughters[0].py())/BRAK;
    HH[2]=AMTAU*(2*PRODNQ*QQ[3]-QQ2*tau_daughters[0].pz())/BRAK;
    HH[3]=1.0;
  }

  // tau^- --> e^- anti_nu_e      nu_tau
  // tau^+ --> e^+      nu_e anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 11,-12) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-11, 12) )
           )
         ) {
    DEBUG( cout<<"Channel 1  : "; )
    channel = 1;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]={0},XA[4] nu_e, QP[4] e, XN[4] neutrino tauowe, AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 0;
    double XK0DEC = 0.01;
    double XK[4] = { 0.0 };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };

    // We fix 4-momenta of electron and electron neutrino
    // Since electrons have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.511e-3*0.511e-3);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );

    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT;  // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> e^- anti_nu_e      nu_tau + gamma
  // tau^+ --> e^+      nu_e anti_nu_tau + gamma
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 11,-12, 22) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-11, 12, 22) )
           )
         ) {
    DEBUG( cout<<"Channel 1b : "; )
    channel = 1;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]  gamma, XA[4] nu_e, QP[4] e, XN[4] neutrino tau , AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 1;
    double XK0DEC = 0.01;
    double XK[4] = { tau_daughters[3].px(), tau_daughters[3].py(), tau_daughters[3].pz(), tau_daughters[3].e() };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };
    
    // We fix 4-momenta of electron and electron neutrino and photon
    // Since electrons have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.511e-3*0.511e-3);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );
    XK[3] = sqrt( XK[0]*XK[0] + XK[1]*XK[1] + XK[2]*XK[2] );
    // XK0DEC must be smaller in TauSpinner  than what was used in generation. We do not use virt. corr anyway.
    if(XK0DEC > XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]))  XK0DEC=0.5*XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]);
    
    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT; // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> mu^- antui_nu_mu      nu_tau
  // tau^+ --> mu^+       nu_mu anti_nu_tau
  else if( pdgid.size()==3 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 13,-14) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-13, 14) )
           )
         ) {

    DEBUG( cout<<"Channel 2  : "; )
    channel = 2;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]={0},XA[4] nu_mu, QP[4] mu, XN[4] neutrino tauowe, AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 0;
    double XK0DEC = 0.01;
    double XK[4] = { 0.0 };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };

    // We fix 4-momenta of muon and muon neutrino
    // Since muon have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.105659*0.105659);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );
    
    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT; // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> mu^- antui_nu_mu      nu_tau + gamma
  // tau^+ --> mu^+       nu_mu anti_nu_tau + gamma
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 13,-14, 22) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,-13, 14, 22) )
           )
         ) {

    DEBUG( cout<<"Channel 2b : "; )
    channel = 2;
    //  ITDKRC=0,XK0DEC=0.01 XK[4]  gamma, XA[4] nu_mu, QP[4] mu, XN[4] neutrino tau, AMPLIT, HH[4]
    //      SUBROUTINE DAMPRY(ITDKRC,XK0DEC,XK,XA,QP,XN,AMPLIT,HV)

    int    ITDKRC = 1;
    double XK0DEC = 0.01;
    double XK[4] = { tau_daughters[3].px(), tau_daughters[3].py(), tau_daughters[3].pz(), tau_daughters[3].e() };
    double XA[4] = { tau_daughters[2].px(), tau_daughters[2].py(), tau_daughters[2].pz(), tau_daughters[2].e() };
    double QP[4] = { tau_daughters[1].px(), tau_daughters[1].py(), tau_daughters[1].pz(), tau_daughters[1].e() };
    double XN[4] = { tau_daughters[0].px(), tau_daughters[0].py(), tau_daughters[0].pz(), tau_daughters[0].e() };
    double AMPLIT = 0.0;
    double HV[4] = { 0.0 };

    // We fix 4-momenta of muon and muon neutrino and photon
    // Since muons have small mass, they are prone to rounding errors
    QP[3] = sqrt( QP[0]*QP[0] + QP[1]*QP[1] + QP[2]*QP[2] + 0.105659*0.105659);
    XA[3] = sqrt( XA[0]*XA[0] + XA[1]*XA[1] + XA[2]*XA[2] );
    XK[3] = sqrt( XK[0]*XK[0] + XK[1]*XK[1] + XK[2]*XK[2] );
    // XK0DEC must be smaller in TauSpinner  than what was used in generation. We do not use virt. corr anyway.
    if(XK0DEC > XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]))  XK0DEC=0.5*XK[3]/(XK[3]+XA[3]+QP[3]+XN[3]);


    dampry_( &ITDKRC, &XK0DEC, XK, XA, QP, XN, &AMPLIT, HV );

    WTamplit = AMPLIT; // WARNING: note XK0DEC dependence is not included in normalisation
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> pi^- pi^0 pi^0 nu_tau
  // tau^+ --> pi^+ pi^0 pi^0 anti_nu_tau
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 111, 111,-211) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 111, 111, 211) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 5;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi0[4], pi0[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    int   MNUM = 0;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4]  = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=2;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> pi^+ pi^- pi^- nu_tau
  // tau^+ --> pi^- pi^+ pi^+ anti_nu_tau
  else if( pdgid.size()==4 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211,-211, 211) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 211,-211) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 5;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    int   MNUM = 0;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  // tau^- --> K^+ pi^- pi^+ nu_tau    // prepared for modes with kaons
  // tau^+ --> K^- pi^- pi^+ anti_nu_tau

  // tau^- --> pi^+ K^- K^- nu_tau    // prepared for modes with kaons
  // tau^+ --> pi^- K^+ K^+ anti_nu_tau
  // tau^- --> K^+ K^- pi^- nu_tau    // prepared for modes with kaons
  // tau^+ --> K^- K^+ pi^+ anti_nu_tau

  // tau^- --> pi^- K^0 pi^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> pi^+ K^0 pi^0 anti_nu_tau


  // tau^- --> pi^- K^0 K^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> pi^+ K^0 K^0 anti_nu_tau


  // tau^- --> K^- K^0 pi^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> K^+ K^0 pi^0 anti_nu_tau

  // tau^- --> K^- pi^0 pi^0 nu_tau    // prepared for modes with kaons
  // tau^+ --> K^+ pi^0 pi^0 anti_nu_tau

   //  3              -3,-1, 3, 0, 0, 0,    -4,-1, 4, 0, 0, 0,  
   //  4              -3, 2,-4, 0, 0, 0,     2, 2,-3, 0, 0, 0,  
   //  5              -3,-1, 1, 0, 0, 0,    -1, 4, 2, 0, 0, 0,  

  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, -211, 321) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321,  211,-321) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 14;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;

   //       IF(I.EQ.14) NAMES(I-7)='  TAU-  -->  K-, PI-,  K+      '
    int   MNUM = 1;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }



  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  311, -211, 311  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  311,  211, 311  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  311, -211, 310  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  311,  211, 310  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  311, -211, 130  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  311,  211, 130  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  310, -211, 311  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  310,  211, 311  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  310, -211, 310  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  310,  211, 310  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  310, -211, 130  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  310,  211, 130  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  130, -211, 311  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  130,  211, 311  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  130, -211, 310  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  130,  211, 310  ) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  130, -211, 130  ) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  130,  211, 130  ) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 15;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
   //       IF(I.EQ.15) NAMES(I-7)='  TAU-  -->  K0, PI-, K0B      '
    int   MNUM = 2;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }



  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, 311,  111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321, 311,  111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, 310,  111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321, 310,  111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, -321, 130,  111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  321, 130,  111) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 16;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
   //       IF(I.EQ.16) NAMES(I-7)='  TAU-  -->  K-,  K0, PI0      '
    int   MNUM = 3;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }



  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,  111,  111,-321) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16,  111,  111, 321) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 17;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
   //       IF(I.EQ.17) NAMES(I-7)='  TAU-  --> PI0  PI0   K-      ''
    int   MNUM = 4;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }


  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-321,-211, 211) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 321, 211,-211) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 18;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    //       IF(I.EQ.18) NAMES(I-7)='  TAU-  -->  K-  PI-  PI+      '
    int   MNUM = 5;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }

  else if( pdgid.size()==4 &&
           (
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 311, 111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 311, 111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 310, 111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 310, 111) ) ||
	    ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211, 130, 111) ) ||
	    ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 130, 111) )
           )
         ) {
    DEBUG( cout<<"Channel 5  : "; )
    channel = 19;
    //  MNUM=0, PT[4] tau, PN[4] neutrino, pi[4], pi[4], pi[4], AMPLIT, HH[4]
    //        CALL DAMPPK(MNUM,PT,PN,PIM1,PIM2,PIPL,AMPLIT,HH)

    const double AMTAU = 1.777;
    //       IF(I.EQ.19) NAMES(I-7)='  TAU-  --> PI-  K0B  PI0      '
    int   MNUM = 6;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    // For RChL currents one needs to define 3-pi sub-channel used
    chanopt_.JJ=1;

    damppk_( &MNUM, PT, PN, PIM1, PIM2, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }
  // tau^- --> pi^+ pi^+ pi^0 pi^- nu_tau
  // tau^+ --> pi^- pi^- pi^0 pi^+ anti_nu_tau
  else if( pdgid.size()==5 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16,-211,-211, 211, 111) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 211, 211,-211, 111) )
           )
         ) {
    DEBUG( cout<<"Channel 8  : "; )
    channel = 8;

    const double AMTAU = 1.777;
    int   MNUM = 1;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIZ [4] = { (float)tau_daughters[4].px(), (float)tau_daughters[4].py(), (float)tau_daughters[4].pz(), (float)tau_daughters[4].e() };
    float PIPL[4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    dam4pi_( &MNUM, PT, PN, PIM1, PIM2, PIZ, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }
  // tau^- --> pi^0 pi^0 pi^0 pi^- nu_tau
  // tau^+ --> pi^0 pi^0 pi^0 pi^+ anti_nu_tau
  else if( pdgid.size()==5 &&
           (
             ( tau_pdgid== 15 && channelMatch(tau_daughters, 16, 111, 111, 111,-211) ) ||
             ( tau_pdgid==-15 && channelMatch(tau_daughters,-16, 111, 111, 111, 211) )
           )
         ) {
    DEBUG( cout<<"Channel 9  : "; )
    channel = 9;

    const double AMTAU = 1.777;
    int   MNUM = 2;
    float PT[4]   = { 0.0, 0.0, 0.0, (float)AMTAU };
    float PN[4]   = { (float)tau_daughters[0].px(), (float)tau_daughters[0].py(), (float)tau_daughters[0].pz(), (float)tau_daughters[0].e() };
    float PIM1[4] = { (float)tau_daughters[1].px(), (float)tau_daughters[1].py(), (float)tau_daughters[1].pz(), (float)tau_daughters[1].e() };
    float PIM2[4] = { (float)tau_daughters[2].px(), (float)tau_daughters[2].py(), (float)tau_daughters[2].pz(), (float)tau_daughters[2].e() };
    float PIZ [4] = { (float)tau_daughters[3].px(), (float)tau_daughters[3].py(), (float)tau_daughters[3].pz(), (float)tau_daughters[3].e() };
    float PIPL[4] = { (float)tau_daughters[4].px(), (float)tau_daughters[4].py(), (float)tau_daughters[4].pz(), (float)tau_daughters[4].e() };
    float AMPLIT = 0.0;
    float HV[4] = { 0.0 };

    dam4pi_( &MNUM, PT, PN, PIM1, PIM2, PIZ, PIPL, &AMPLIT, HV );

    WTamplit = AMPLIT;
    HH[0] = -HV[0];
    HH[1] = -HV[1];
    HH[2] = -HV[2];
    HH[3] =  HV[3];
  }
  else {

    DEBUG( cout<<tau_daughters.size()<<"-part  ???: "; )

  }

  // Now rotate vector HH using angles phi and theta
  Particle HHbuf(HH[0], HH[1], HH[2], HH[3], 0);
  
  HHbuf.rotateXZ(theta);
  HHbuf.rotateXY(phi);
  
  HH[0] = HHbuf.px();
  HH[1] = HHbuf.py();
  HH[2] = HHbuf.pz();
  HH[3] = HHbuf.e ();

  return HH;
}

/*******************************************************************************
 Returns longitudinal polarization of the single tau (in Z/gamma* -> tau+ tau- case) averaged over
 incoming configurations
 S: invariant mass^2 of the bozon
 &sp_tau: first tau
 &sp_nu_tau: second tau  (in this case it is misleading name)
 Hidden output: WTnonSM
 Hidden input:  relWTnonS, nonSM2
*******************************************************************************/
double getLongitudinalPolarization(double S, SimpleParticle &sp_tau, SimpleParticle &sp_nu_tau)
{
  // tau+ and tau- in lab frame
  Particle tau_plus (    sp_tau.px(),    sp_tau.py(),    sp_tau.pz(),    sp_tau.e(),    sp_tau.pdgid() );
  Particle tau_minus( sp_nu_tau.px(), sp_nu_tau.py(), sp_nu_tau.pz(), sp_nu_tau.e(), sp_nu_tau.pdgid() );

  // P_QQ = sum of tau+ and tau- in lab frame
  Particle P_QQ( tau_plus.px()+tau_minus.px(), tau_plus.py()+tau_minus.py(), tau_plus.pz()+tau_minus.pz(), tau_plus.e()+tau_minus.e(), 0 );
  
  Particle P_B1(0, 0, 1, 1, 0);
  Particle P_B2(0, 0,-1, 1, 0);

  tau_plus. boostToRestFrame(P_QQ);
  tau_minus.boostToRestFrame(P_QQ);
  P_B1.     boostToRestFrame(P_QQ);
  P_B2.     boostToRestFrame(P_QQ);
  
  double costheta1 = (tau_plus.px()*P_B1.px()    +tau_plus.py()*P_B1.py()    +tau_plus.pz()*P_B1.pz()    ) /
                 sqrt(tau_plus.px()*tau_plus.px()+tau_plus.py()*tau_plus.py()+tau_plus.pz()*tau_plus.pz()) /
                 sqrt(P_B1.px()    *P_B1.px()    +P_B1.py()    *P_B1.py()    +P_B1.pz()    *P_B1.pz()    );

  double costheta2 = (tau_minus.px()*P_B2.px()    +tau_minus.py()*P_B2.py()    +tau_minus.pz()*P_B2.pz()    ) /
                 sqrt(tau_minus.px()*tau_minus.px()+tau_minus.py()*tau_minus.py()+tau_minus.pz()*tau_minus.pz()) /
                 sqrt(P_B2.px()    *P_B2.px()    +P_B2.py()    *P_B2.py()    +P_B2.pz()    *P_B2.pz()    );
               
  double sintheta1 = sqrt(1-costheta1*costheta1);
  double sintheta2 = sqrt(1-costheta2*costheta2);
  
  // Cosine of hard scattering
  double costhe = (costheta1*sintheta2 + costheta2*sintheta1) / (sintheta1 + sintheta2);
  
  // Invariant mass^2 of, tau+tau- pair plus photons, system!
  double SS     = S; // other option is for tests: P_QQ.recalculated_mass()*P_QQ.recalculated_mass();
  
  /*
    We need to fix sign of costhe and attribute ID. 
    calculate x1,x2;  // x1*x2 = SS/CMSENE/CMSENE; // (x1-x2)/(x1+x2)=P_QQ/CMSENE*2 in lab; 
    calculate weight WID[]=sig(ID,SS,+/-costhe)* f(x1,+/-ID,SS) * f(x2,-/+ID,SS) ; respectively for u d c s b 
    f(x,ID,SS,CMSENE)=x*(1-x) // for the start it will invoke library
    on the basis of this generate ID and set sign for costhe. 
    then we calculate polarization averaging over incoming states.
  */
  
  double x1x2  = SS/CMSENE/CMSENE;
  double x1Mx2 = P_QQ.pz()/CMSENE*2;
  
  double x1 = (  x1Mx2 + sqrt(x1Mx2*x1Mx2 + 4*x1x2) )/2;
  double x2 = ( -x1Mx2 + sqrt(x1Mx2*x1Mx2 + 4*x1x2) )/2;
  
  double WID[11];
  WID[0] = f(x1, 0,SS,CMSENE)*f(x2, 0,SS,CMSENE) * sigborn(0,SS, costhe);
  WID[1] = f(x1, 1,SS,CMSENE)*f(x2,-1,SS,CMSENE) * sigborn(1,SS, costhe);
  WID[2] = f(x1,-1,SS,CMSENE)*f(x2, 1,SS,CMSENE) * sigborn(1,SS,-costhe);
  WID[3] = f(x1, 2,SS,CMSENE)*f(x2,-2,SS,CMSENE) * sigborn(2,SS, costhe);
  WID[4] = f(x1,-2,SS,CMSENE)*f(x2, 2,SS,CMSENE) * sigborn(2,SS,-costhe);
  WID[5] = f(x1, 3,SS,CMSENE)*f(x2,-3,SS,CMSENE) * sigborn(3,SS, costhe);
  WID[6] = f(x1,-3,SS,CMSENE)*f(x2, 3,SS,CMSENE) * sigborn(3,SS,-costhe);
  WID[7] = f(x1, 4,SS,CMSENE)*f(x2,-4,SS,CMSENE) * sigborn(4,SS, costhe);
  WID[8] = f(x1,-4,SS,CMSENE)*f(x2, 4,SS,CMSENE) * sigborn(4,SS,-costhe);
  WID[9] = f(x1, 5,SS,CMSENE)*f(x2,-5,SS,CMSENE) * sigborn(5,SS, costhe);
  WID[10]= f(x1,-5,SS,CMSENE)*f(x2, 5,SS,CMSENE) * sigborn(5,SS,-costhe);
  
  double sum = 0.0;  // normalize
  for(int i=0;i<=10;i++) sum+=WID[i];
  
  if( sum == 0.0 )
  {
    cout << "Tauspinner::calculateWeightFromParticlesH WARNING: sum of WID[0]-WID[10] is 0. Check LHAPDF configuration" << endl;
  }

  WTnonSM=1.0;
  if(relWTnonSM==0)  WTnonSM=sum;
  if(nonSM2==1)
  {
    double WID2[11];
    WID2[0] = f(x1, 0,SS,CMSENE)*f(x2, 0,SS,CMSENE) * sigborn(0,SS, costhe) * plweight(0,SS, costhe); // plweight = ratio of cross-section nonSM/SM
    WID2[1] = f(x1, 1,SS,CMSENE)*f(x2,-1,SS,CMSENE) * sigborn(1,SS, costhe) * plweight(1,SS, costhe);
    WID2[2] = f(x1,-1,SS,CMSENE)*f(x2, 1,SS,CMSENE) * sigborn(1,SS,-costhe) * plweight(1,SS,-costhe);
    WID2[3] = f(x1, 2,SS,CMSENE)*f(x2,-2,SS,CMSENE) * sigborn(2,SS, costhe) * plweight(2,SS, costhe);
    WID2[4] = f(x1,-2,SS,CMSENE)*f(x2, 2,SS,CMSENE) * sigborn(2,SS,-costhe) * plweight(2,SS,-costhe);
    WID2[5] = f(x1, 3,SS,CMSENE)*f(x2,-3,SS,CMSENE) * sigborn(3,SS, costhe) * plweight(3,SS, costhe);
    WID2[6] = f(x1,-3,SS,CMSENE)*f(x2, 3,SS,CMSENE) * sigborn(3,SS,-costhe) * plweight(3,SS,-costhe);
    WID2[7] = f(x1, 4,SS,CMSENE)*f(x2,-4,SS,CMSENE) * sigborn(4,SS, costhe) * plweight(4,SS, costhe);
    WID2[8] = f(x1,-4,SS,CMSENE)*f(x2, 4,SS,CMSENE) * sigborn(4,SS,-costhe) * plweight(4,SS,-costhe);
    WID2[9] = f(x1, 5,SS,CMSENE)*f(x2,-5,SS,CMSENE) * sigborn(5,SS, costhe) * plweight(5,SS, costhe);
    WID2[10]= f(x1,-5,SS,CMSENE)*f(x2, 5,SS,CMSENE) * sigborn(5,SS,-costhe) * plweight(5,SS,-costhe);
    
    double sum2 = 0.0;  // normalize
    for(int i=0;i<=10;i++) sum2+=WID2[i];

    WTnonSM=sum2/sum ; 
    if(relWTnonSM==0)  WTnonSM=sum2;
    }
  
  double pol = 0.0;
  //  double Rxx = 0.0;
  //  double Ryy = 0.0;
  
  if(IfHiggs && nonSM2==1) {   // we assume that only glue glue process contributes for Higgs
    double polp = plzap2(0,15,S,costhe);  // 0 means incoming gluon, 15 means outgoing tau
    pol += (2*(1-polp)-1);
     return pol;
  }
  if(IfHiggs) return NAN;

  // case of Z/gamma 
  for(int i=0;i<=10;i++) WID[i]/=sum;


  R11 = 0.0;
  R22 = 0.0;

  int ICC = -1;
  
  for(int i=1;i<=10;i++)
  {

    ICC = i;
    double cost = costhe;
    // first beam quark or antiquark

    if( ICC==2 || ICC==4 || ICC==6 || ICC==8 || ICC==10 )  cost = -cost;

    // ID of incoming quark (up or down type)
    int                     ID = 2;          
    if( ICC==7 || ICC==8  ) ID = 4;
    if( ICC==1 || ICC==2  ) ID = 1;
    if( ICC==5 || ICC==6  ) ID = 3;
    if( ICC==9 || ICC==10 ) ID = 5;

    int tau_pdgid = 15;

    double polp = plzap2(ID,tau_pdgid,S,cost);
    pol += (2*(1-polp)-1)*WID[i];

    // we obtain transverse spin components of density matrix from Tauolapp pre-tabulated O(alpha) EW results
    //  
    Tauolapp::TauolaParticlePair pp;

    // Set them to 0 in case no tables are loaded by Tauola++
    pp.m_R[1][1] = pp.m_R[2][2] = 0.0;

    pp.recalculateRij(ID,tau_pdgid,S,cost);

    // These calculations are valid for Standard Model only
    R11 += WID[i]*pp.m_R[1][1];
    R22 += WID[i]*pp.m_R[2][2];
  }
  
  // Calculations are prepared only for pp collision.
  // Otherwise pol = 0.0
  if(!Ipp) pol=0.0;

  return pol;
}

/*******************************************************************************
  Check if pdg's of particles in the vector<Particle>&particles  match the 
  of (p1,p2,p3,p4,p5,p6)

  Returns true if 'particles' contain all of the listed pdgid-s.
  If it does - vector<Particle>&particles will be sorted in the order  
  as listed pdgid-s.

  It is done so the order of particles is the same as the order used by
  TAUOLA Fortran routines.
*******************************************************************************/
bool channelMatch(vector<Particle> &particles, int p1, int p2, int p3, int p4, int p5, int p6)
{
  // Copy pdgid-s of all particles
  vector<int> list;
  
  for(unsigned int i=0;i<particles.size();i++) list.push_back(particles[i].pdgid());
  
  // Create array out of pdgid-s
  int p[6] = { p1, p2, p3, p4, p5, p6 };

  // 1) Check if 'particles' contain all pdgid-s on the list 'p'
  
  for(int i=0;i<6;i++)
  {
    // if the pdgid is zero - finish
    if(p[i]==0) break;
    
    bool found = false;
    
    for(unsigned int j=0;j<list.size(); j++)
    {
      // if pdgid is found - erese it from the list and search for the next one
      if(list[j]==p[i])
      {
        found = true;
        list.erase(list.begin()+j);
        break;
      }
    }
    
    if(!found) return false;
  }
  
  // if there are more particles on the list - there is no match
  if(list.size()!=0) return false;

  
  // 2) Rearrange particles to match the order of pdgid-s listed in array 'p'

  vector<Particle> newList;
  
  for(int i=0;i<6;i++)
  {
    // if the pdgid is zero - finish
    if(p[i]==0) break;
    
    for(unsigned int j=0;j<particles.size(); j++)
    {
      // if pdgid is found - copy it to new list and erese from the old one
      if(particles[j].pdgid()==p[i])
      {
        newList.push_back(particles[j]);
        particles.erase(particles.begin()+j);
        break;
      }
    }
  }
  
  particles = newList;

  return true;
}

/*******************************************************************************
 Prints out two vertices:
   W   -> tau, nu_tau
   tau -> tau_daughters
*******************************************************************************/
void print(Particle &W, Particle &nu_tau, Particle &tau, vector<Particle> &tau_daughters) {

  nu_tau.print();
  tau   .print();

  double px=nu_tau.px()+tau.px();
  double py=nu_tau.py()+tau.py();
  double pz=nu_tau.pz()+tau.pz();
  double e =nu_tau.e ()+tau.e ();

  // Print out  sum of tau and nu_tau  and also  W  momentum for comparison
  cout<<"--------------------------------------------------------------------------------------------------------"<<endl;
  Particle sum1(px,py,pz,e,0);
  sum1.print();
  W   .print();

  cout<<endl;

  // Print out tau daughters
  for(unsigned int i=0; i<tau_daughters.size();i++) tau_daughters[i].print();

  // Print out sum of tau decay products, and also tau momentum for comparison
  cout<<"--------------------------------------------------------------------------------------------------------"<<endl;
  Particle *sum2 = vector_sum(tau_daughters);
  sum2->print();
  tau.print();
  cout<<endl;
  
  delete sum2;
}

/*******************************************************************************
 Sums all 4-vectors of the particles on the list
*******************************************************************************/
Particle *vector_sum(vector<Particle> &x) {

  double px=0.0,py=0.0,pz=0.0,e=0.0;

  for(unsigned int i=0; i<x.size();i++)
  {
    px+=x[i].px();
    py+=x[i].py();
    pz+=x[i].pz();
    e +=x[i].e();
  }

  Particle *sum = new Particle(px,py,pz,e,0);
  return sum;
}

} // namespace TauSpinner

