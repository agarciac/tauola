#ifndef _TAU_REWEIGHT_LIB_H_
#define _TAU_REWEIGHT_LIB_H_

// Debug mode
#ifdef DEBUG_MODE
#define DEBUG(arg) arg;
#else
#define DEBUG(arg)
#endif

// TAUOLA header
#include "Tauola/Tauola.h"

#include "TauSpinner/Tauola_wrapper.h"
#include "TauSpinner/SimpleParticle.h"
#include "TauSpinner/Particle.h"

// LHAPDF header
#include "LHAPDF/LHAPDF.h"

#include <vector>
#include <functional> //ATLAS: Added
#include <iostream>
using std::vector;
using std::cout;
using std::endl;

namespace TauSpinner {

// ==========================================================
// ATLAS: Added new class to encapsulate TauSpinner globals
// ==========================================================
class TauSpinnerWeighter
{

public: 
// constructor 
TauSpinnerWeighter();
// new function pointers
private:
  std::function<double(int, double, double, int, int, int)> nonSM_bornZ;
  std::function<double(int, double, double, int, int, int)> nonSM_bornH;
  std::function<void(double, int, int)> alphasModif;
  std::function<double(int, int, int, int, int, int, double[6][4], int, double)> vbfdistrModif;

public:
// functions copied from tau_reweight_lib.h
void initialize_spinner(bool _Ipp, int _Ipol, int _nonSM2, int _nonSMN, double _CMSENE);
void setRelWTnonSM(int _relWTnonSM);
 void setHiggsParameters(int jak, double mass, double width, double normalization);
void getHiggsParameters(double *mass, double *width, double *normalization);
void setSpinOfSample(int _Ipol);
void setNonSMkey(int key);
void setHiggsParametersTR(double Rxx, double Ryy, double Rxy, double Ryx);
void setZgamMultipliersTR(double Rxx, double Ryy, double Rxy, double Ryx);
void getZgamParametersTR(double &Rxx, double &Ryy, double &Rxy, double &Ryx);
double getWtNonSM();
double getWtamplitP();
double getWtamplitM();
double getTauSpin();
double calculateWeightFromParticlesWorHpn(SimpleParticle &W, SimpleParticle &tau, SimpleParticle &nu_tau, vector<SimpleParticle> &tau_daughters);
double calculateWeightFromParticlesH(SimpleParticle &sp_X, SimpleParticle &sp_tau1, SimpleParticle &sp_tau2, vector<SimpleParticle> &sp_tau1_daughters, vector<SimpleParticle> &sp_tau2_daughters);
 SimpleParticle getHHp(){return HHp_;};
 SimpleParticle getHHm(){return HHm_;};
void prepareKinematicForHH(Particle &tau, Particle &nu_tau, vector<Particle> &tau_daughters, double *phi2, double *theta2);
double* calculateHH(int tau_pdgid, vector<Particle> &tau_daughters, double phi, double theta);
double getLongitudinalPolarization(double S, SimpleParticle &sp_tau, SimpleParticle &sp_nu_tau);
bool channelMatch(vector<Particle> &particles, int p1, int p2=0, int p3=0, int p4=0, int p5=0, int p6=0);
void print(Particle &W, Particle &nu_tau, Particle &tau, vector<Particle> &tau_daughters);
Particle *vector_sum(vector<Particle> &x);
// undeclared functions copied from tau_reweight_lib.cxx
double f(double x, int ID, double SS, double cmsene);
double sigborn(int ID, double SS, double costhe);

public:
// functions copied from nonSM.h
double default_nonSM_born(int ID, double S, double cost, int H1, int H2, int key);
double default_nonSM_bornH(int ID, double S, double cost, int H1, int H2, int key);
double nonSM_born(int ID, double S, double cost, int H1, int H2, int key);
void set_nonSM_born( std::function<double(int, double, double, int, int, int)> fun );
void set_nonSM_bornH( std::function<double(int, double, double, int, int, int)> fun );
double plzap2(int ide, int idf, double svar, double costhe);
double plweight(int ide, double svar, double costhe);
double plnorm(int ide, double svar);
void nonSMHcorrPol(double S, SimpleParticle &tau1, SimpleParticle &tau2,
                   double *corrX2, double *polX2);


public:
// functions copied from vbfdistr.h
//double vbfdistr(int I1, int I2, int I3, int I4, int H1, int H2, SimpleParticle &p1, SimpleParticle &p2, SimpleParticle &tau1, SimpleParticle &tau2, int KEY); // AFAIK this function is never defined
double vbfdistr(int I1, int I2, int I3, int I4, int H1, int H2, double P[6][4], int KEY);
void setPDFOpt(int QCDdefault, int QCDvariant);
void set_vbfdistrModif(std::function<double(int, int, int, int, int, int, double[6][4], int, double)> function );
void set_alphasModif(std::function<void(double, int, int)> function );
void getME2VBF(SimpleParticle &p3, SimpleParticle &p4, SimpleParticle &sp_X,SimpleParticle &tau1, SimpleParticle &tau2, double (&W)[2][2], int KEY);
double calculateWeightFromParticlesVBF(SimpleParticle &p3, SimpleParticle &p4,SimpleParticle &sp_X, SimpleParticle &sp_tau1, SimpleParticle &sp_tau2, vector<SimpleParticle> &sp_tau1_daughters, vector<SimpleParticle> &sp_tau2_daughters);

// undeclared functions copied from VBF/vbfdistr.cxx 
int getPDFOpt(int KEY);
void  alphas(double Q2, int scalePDFOpt, int KEY);


private:
// former global variables copied from tau_reweight_lib.cxx
double CMSENE;         // Center of mass system energy (used by PDF's) 
bool   Ipp;            // pp collisions
int    Ipol;           // Is the  sample in use polarized? (relevant for Z/gamma case only)
int    nonSM2;         // Turn on/off nonSM calculations 
int    nonSMN;         // Turn on, if calculations of nonSM weight, is to be used to modify shapes only
int    relWTnonSM;     // 1: relWTnonSM is relative to SM; 0: absolute
double WTnonSM;        // nonSM weight
double Polari;         // Helicity, attributed to the tau pair. If not attributed then 0.
                       // Polari is attributed for the case when spin effects are taken into account.
                       // Polari is available for the user program with getTauSpin() 
bool IfHiggs;          // flag used in  sigborn()
double WTamplit;       // AMPLIT weight for the decay last invoked
double WTamplitP;      // AMPLIT weight for tau+ decay
double WTamplitM;      // AMPLIT weight for tau- decay

// Higgs parameters
int  IfHsimple;        // switch for simple Higgs (just Breit-Wigner)
double XMH;            // mass (GeV)
double XGH;            // width, should be detector width, analysis dependent.
double Xnorm;          // normalization of Higgs Born cross-section, at hoc

// Transverse components of Higgs spin density matrix
double RXX;       //-1.0;
double RYY;       // 1.0;
double RXY;      
double RYX;       

//Polarimetric vectors
 SimpleParticle HHm_;
 SimpleParticle HHp_;


// Coefficients for transverse components of Z/gamma spin density matrix
double RzXX;       //-1.0;
double RzYY;       // 1.0;
double RzXY;      
double RzYX;       

// Values of transverse components of Z/gamma spin density matrix calculated inside getLongitudinalPolarization
double R11;       
double R22;       
double R12;        // for future use 
double R21;        // for future use


// former global variables copied from VBF/vbfdistr.cxx
bool DEBUGVBF; // had to change name to avoid conflict with pre-comp macro 'DEBUG'
int _QCDdefault;
int _QCDvariant;

}; 
// ==========================================================
// ATLAS: End class TauSpinnerWeighter, resume original code
//        left only so the package will compile 
// ==========================================================


/** Definition of REAL*8 FUNCTION DISTH(S,T,H1,H2) from disth.f calculating 
    SM glue glue-->Higgs --> tau tau*/
extern "C" double disth_(double *SVAR, double *COSTHE, int *TA, int *TB);

/** Initialize TauSpinner

    Print info and set global variables */
void initialize_spinner(bool _Ipp, int _Ipol, int _nonSM2, int _nonSMN, double _CMSENE);

/** Set flag for calculating relative (NONSM/SM) or absolute that is matrix element^2 (spin averaged), 
  weight for X-section calculated as by product in longitudinal polarization method. */
void setRelWTnonSM(int _relWTnonSM);

/** set flag for simple formula (just Breit Wigner) of Higgs cross section, 
    set its parameters: Higgs mass, width and normalization for Higgs born (BW) function */
 void setHiggsParameters(int jak, double mass, double width, double normalization);

/** Get Higgs mass, width and normalization of Higgs born function */
void getHiggsParameters(double *mass, double *width, double *normalization);

/**  Set flag defining what type of spin treatment was used in analyzed  sample */
void setSpinOfSample(bool _Ipol);

/**  Turn  non Standard Model calculation  on/off */
void setNonSMkey(int key);

/** set transverse components for Higgs spin density matrix */
void setHiggsParametersTR(double Rxx, double Ryy, double Rxy, double Ryx);

/** set multipliers for transverse components  for Drell-Yan spin density matrix */
void setZgamMultipliersTR(double Rxx, double Ryy, double Rxy, double Ryx);

/** get transverse components  for Drell-Yan spin density matrix */
void getZgamParametersTR(double &Rxx, double &Ryy, double &Rxy, double &Ryx);

/** Get nonSM weight of production matrix element */
double getWtNonSM();

/** Get tau+ weight of its decay matrix element */
double getWtamplitP();

/** Get tau- weight of its decay matrix element */
double getWtamplitM();

/** Get tau Helicity
    Used after event  is analyzed to obtain  attributed tau helicity.
    Returns -1 or 1. Note that 0 is returned if attribution fails. Method
    assumes that sample has spin effects taken into account. Effects can 
    be taken into account with the help of spin weights. */
double getTauSpin();

/** Calculate weights.

  Determines decay channel, calculates all necessary components for 
  calculation of all weights, calculates weights.
  Function for W+/- and H+/-  */
double calculateWeightFromParticlesWorHpn(SimpleParticle &W, SimpleParticle &tau, SimpleParticle &nu_tau, vector<SimpleParticle> &tau_daughters);

/** Calculate weights.

  Determines decay channel, calculates all necessary components for 
  calculation of all weights, calculates weights.
  Function for H/Z */
double calculateWeightFromParticlesH(SimpleParticle &sp_X, SimpleParticle &sp_tau1, SimpleParticle &sp_tau2, vector<SimpleParticle> &sp_tau1_daughters, vector<SimpleParticle> &sp_tau2_daughters);

/** Prepare kinematics for HH calculation
  
  Boost particles to effective pair rest frame, and rotate them so that tau is on Z axis.
  Then rotate again with theta2 phi2 so neutrino (first tau decay product) from tau decay is along Z. */
void prepareKinematicForHH(Particle &tau, Particle &nu_tau, vector<Particle> &tau_daughters, double *phi2, double *theta2);

/** Calculate polarization vector.

  We use FORTRAN metdods to calculate HH.
  First decide what is the channel. After that, 4-vectors
  are moved to tau rest frame of tau.
  Polarimetric vector HH is then rotated using angles phi and theta.
  
  Order of the particles does not matter. */
double* calculateHH(int tau_pdgid, vector<Particle> &tau_daughters, double phi, double theta);

/**
 Get Longitudinal polarization
 
 Returns longitudinal polarization in Z/gamma* -> tau+ tau-
 S: invariant mass^2 of the bozon */
double getLongitudinalPolarization(double S, SimpleParticle &sp_tau, SimpleParticle &sp_nu_tau);

/** Check if the vector of particles match the listed order of pdgid's:

  1) Returns true if 'particles' contain all of the listed pdgid-s.
  2) If it does - 'particles' will be reordered so that they have
     the same order as listed pdgid-s.

  It is done so the order of particles is the same as the order mandatory
  for TAUOLA Fortran routines. */
bool channelMatch(vector<Particle> &particles, int p1, int p2=0, int p3=0, int p4=0, int p5=0, int p6=0);

//------------------------------------------------------------------------------
//-- Useful debug methods ------------------------------------------------------
//------------------------------------------------------------------------------

/** Prints out two vertices:
      W   -> tau, nu_tau
      tau -> tau_daughters */
void print(Particle &W, Particle &nu_tau, Particle &tau, vector<Particle> &tau_daughters);

/** Sums all 4-vectors of the particles on the list */
Particle *vector_sum(vector<Particle> &x);

} // namespace TauSpinner
#endif
